<?
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	echo "<pre>";//debug
//	print_r($_POST);
	echo "This form is not functional yet. 2009_12_14 il"; exit; //debug 

	echo "</pre>";//debug
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>
			Join - The National Geospatial Digital Archive (NGDA)

</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="stylesheets/fluid.gs.css" type="text/css" media="screen" charset="utf-8" />
<!--[if lt IE 8]><link rel="stylesheet" href="stylesheets/fluid.gs.lt_ie8.css" type="text/css" media="screen" title="no title" charset="utf-8"><![endif]-->
<link href="stylesheets/ngda.css" media="screen" rel="stylesheet" charset="utf-8" type="text/css" />
	<link rel="stylesheet" href="qchange/css/qc_css.css" />
<!-- load Jquery library from Google api -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="qchange/js/quickchange.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div class="fluid_grid_layout"> 
<!-- Header -->
<div class="six_column section">
	<div class="four column mid">
	<div class="column_content">
		<h1 class="">The National Geospatial Digital Archive</h1>
<!-- InstanceBeginEditable name="subhead" -->
		<h2 id="subhead">A Collaborative Initiative</h2>
<!-- InstanceEndEditable -->
		</div>
	</div> 
	<div class="two column">
	<div class="column_content">
		<a href="home.html" class="plain"><img src="images/ngda_logo.png" alt="NGDA Logo" width="170" height="140" /></a></div>
	</div> 
</div>



<div class="six_column section">
<!-- Left Navigation -->
	<div class="two column">
    <div class="column_content" id="navigation">
		<ul>
			<li><a href="home.html">Home</a></li>
		  
			<li><a href="research.html">Research</a></li>
			<li><a href="reports.html">Reports</a></li>
			<li><a href="join.php">Join</a></li>
		  <li><a href="contact.html">Contact Us</a></li>
			<li><a href="participants.html">Federation Members</a></li>
		  <li><a href="project_archive.html">Project Archive</a></li>
			
	      <br />&nbsp;
		</ul>
	</div>
<div id="leftlower" class="round">
<div id="partnerLogos">
<ul>
	<li><a class="plain" title="University of California Santa Barbara"><img src="images/ucsb_logo.png" width="68" height="73" /></a></li>
	<li><a class="plain" title="Stanford University"><img src="images/stanford_logo_t.png" width="69" height="73" /></a></li>
	<li><a class="plain" title="Library of Congress"><img src="images/library_of_congress_logo.png" width="75" height="73" alt="Library of Congress Logo" /></a></li>
	<li><a class="plain" title="University of Tennessee"><img src="images/u_of_tennessee_logo.png" width="85" height="61" /></a></li>
	<li><a class="plain" title="Vanderbuilt University"><img src="images/vanderbuilt_university_logo.png" width="64" height="53" /></a></li>
</ul>
</div>
</div>

	</div><!-- end two column div -->


<!--  Main Content -->
	<div class="five column">
	<div class="column_content">
	<div id="main_content">
<!-- InstanceBeginEditable name="Content" -->
<h1>Join NGDA</h1>

<p>
If you would like to jon the NGDA Federation please contact us with the form below:<br />
</p>
<form action="join.php" method="post" enctype="application/x-www-form-urlencoded" name="contactus_form" id="contactus_form">
  <label for="name">Name</label>
  <input type="text" name="name" id="name" tabindex="10" class="round" />
  <br />
  <label for="institution">Institution</label>
  <input type="text" name="institution" id="institution" tabindex="20"  class="round" />
  <br />
  <label for="email">Email</label>
  <input type="text" name="email" id="email" tabindex="30"  class="round" />
  <br />
  <label for="comments">Comments</label>
  <textarea name="comments" id="comments" cols="45" rows="5" tabindex="40" class="round" ></textarea>
  <br />
  <label for="non-fill"></label>
  <input type="text" name="non-fill" id="non-fill" />
  <br />

<input type="submit" name="submit" id="submit" value="Submit" tabindex="50" />
</form>

<br />
<br />
<br />

<p><a href="policies.html">Policies & Contracts</a></p>





<!-- InstanceEndEditable -->
	</div>
	</div>
	</div>
</div>


<!-- Footer -->
<div class="six_column section">
	<div class="six column">
		<div class="column_content">
		<div class="footer">
Copyright <a class="plain" href="javascript:login_to_QC();">&copy;</a> 2005-2009 University of California, Santa Barbara, CA 93106</div>
		</div>
	</div> 
</div>










</div>
</body>
<!-- InstanceEnd --></html>
