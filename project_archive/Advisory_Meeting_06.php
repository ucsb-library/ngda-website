<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Advisory Board Meeting</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style59 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.style60 {	font-size: 16px;
	font-weight: bold;
	color: #000033;
}
.style61 {font-size: 12px}
.style64 {font-family: Arial, Helvetica, sans-serif}
.style66 {color: #000033; font-family: Arial, Helvetica, sans-serif; font-size: 14px;}
.style67 {color: #000033; font-size: 14px;}
body {
	margin-left: 15px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
</head>

<body>
<table width="100%" height="100%" align="left" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5" valign="top"><img src="../images/templates/Phase II Banner.jpg" width="760" height="162"></td>
    <td width="211" rowspan="4" align="right" valign="top" bgcolor="#4F5151">
      <div align="left"><img src="../Templates/left banner copy.jpg" width="210" height="593"></div></td>
  </tr>
  <tr>
    <td width="109" bgcolor="#FFFFFF"><div align="center"><a href="../index.php">Home</a></div></td>
    <td width="109" bgcolor="#FFFFFF"><div align="center"><a href="../research.php" name="link4" id="link2" onMouseOver="MM_showMenu(window.mm_menu_0911125913_0,0,15,null,'link4')" onMouseOut="MM_startTimeout();">Research</a></div></td>
    <td width="108" bgcolor="#FFFFFF"><div align="center"><a href="../meetings.php">Meetings</a></div></td>
    <td width="109" bgcolor="#FFFFFF"><div align="center"><a href="../participants.php" name="link3" id="link1" onMouseOver="MM_showMenu(window.mm_menu_0911125648_0,0,15,null,'link3')" onMouseOut="MM_startTimeout();">Participants</a></div></td>
    <td width="107" bgcolor="#FFFFFF"><div align="center"><a href="../contact.php"> &nbsp;Contacts</a></div></td>
  </tr>
  <tr>
    <td height="271" colspan="5" valign="top"><p align="left" class="style59">&nbsp;</p>
        <p align="left" class="style59"><span class="style60"><u>Executive Advisory Board Meeting<br>
        </u></span><span class="style67">November 29-30, 2006<br>
        San Francisco, CA </span><br>
        </p>
        <p>We would now like to convene an external group of colleagues to help us look into the short-term future for guidance on policies and governance structures as we begin to increase the network of repositories archiving geospatial data. We are interested in discussing the following broad topics: </p>
        <ul>
          <li>How do we attract others into the network? What do we expect repository partners to be able to do? Should there be levels of participation? What are the guidelines that should be created for those wanting to simply deposit data rather than be a node? </li>
          <li>What alliances should we try to form with: existing geospatial networks, government agencies (federal and state), commercial companies, individuals, etc.? </li>
          <li>Do we need a long-term governance structure? What are viable alternatives to that? </li>
        </ul>        <p align="left" class="style59"><br>
          <br>
        </p>
        <div align="left"><div align="left"></div>
      </div></td>
  </tr>
  <tr>
    <td height="62" colspan="5" valign="top"><img src="../images/Templates/bottom_title copy.jpg" width="550" height="38"></td>
  </tr>
</table>
<div align="left"></div>
</body>
</html>
