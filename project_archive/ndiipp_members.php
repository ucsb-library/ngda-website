<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	margin-left: 15px;
	margin-top: 0px;
}
a {
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
	color: #000033;
}
.style183 {font-size: 12px; color: #000000; }
.style184 {font-size: 12px}
.style187 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.style196 {font-family: Arial, Helvetica, sans-serif}
.style94 {	font-size: 14px;
	color: #000066;
	font-weight: bold;
}
a:link {
	text-decoration: none;
	color: #000066;
}
a:visited {
	text-decoration: none;
	color: #000099;
}
a:hover {
	text-decoration: underline;
	color: #0000FF;
}
a:active {
	text-decoration: none;
}
.style200 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #000000; }
.style1 {color: #000000}
.style174 {font-size: 14px}
.style210 {font-size: 14px; font-family: Arial, Helvetica, sans-serif; }
.style211 {
	color: #000099;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
}
.style122 {font-size: 18px; color: #000000; font-weight: bold;}
.style179 {color: #000000; font-size: 14px; }
.style234 {color: #000066; font-size: 16px;}
.style185 {font-size: 14px; font-weight: bold; color: #000000; }
.style189 {color: #000000; font-size: 18px;}
.style191 {font-family: "Times New Roman", Times, serif; font-size: 16px;}
.style192 {color: #000066; font-size: 16px; font-weight: bold; }
.style214 {font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #000066;}
.style222 {font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #000000; font-size: 12px; }
.style227 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
.style229 {font-size: 12px; font-weight: bold; }
.style233 {font-size: 12px; color: #000000; font-weight: bold; }
-->
</style>
</head>

<body>
<div align="left"></div>
<div align="left">
  <table width="760" height="100%" align="left" cellpadding="0" cellspacing="0">
    <tr>
      <td height="35" colspan="5" valign="top"><p><img src="../images/templates/New Banner.jpg" width="458" height="35"></p></td>
      <td width="230" rowspan="4" align="right" valign="top" bgcolor="#4F5151"> <img src="../images/ngda_vert_banner.jpg" width="210" height="593"></td>
    </tr>
    <tr>
      <td width="105" height="21" bgcolor="#FFFFFF"><div align="left"><a href="../index.php">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.php">&nbsp;</a></div></td>
      <td width="127" height="21" bgcolor="#FFFFFF"><div align="left"><a href="../research.php">Research</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../research.php">&nbsp;</a></div></td>
      <td width="121" height="21" bgcolor="#FFFFFF"><div align="left"><a href="../meetings.php">Meetings</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../meetings.php">&nbsp;</a></div></td>
      <td width="143" height="21" bgcolor="#FFFFFF"><div align="left"><a href="../participants.php">Participants</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
      <td width="53" height="21" bgcolor="#FFFFFF"><div align="left"> &nbsp;<a href="../contact.php">Contacts</a></div></td>
    </tr>
    <tr>
      <td height="271" colspan="5" align="left" valign="top"><p align="left" class="style187">&nbsp;</p>
          <p align="left" class="style187">&nbsp;</p>
          <p class="style174"><span class="style210"><span class="style184"><span class="style187">In addition to the NGDA team, other NDIIPP &nbsp;partners contribute the knowledge they have gained through working with like institutions.</span></span><br>
</span></p>
          <p class="style174"><u><span class="style211">NDIIPP Partners</span></u></p>
          <p class="style174">&nbsp;</p>
          <p><span class="style200">North Carolina State University</span><br>
              <span class="style183"><a href="http://www.lib.ncsu.edu/ncgdap/index.html" class="style184">North Carolina Geospatial Data Archiving Project<br>
              </a></span><br>
              <span class="style200">Educational Broadcasting Corp./Thirteen WNET-TV</span><span class="style183"><br>
              <a href="http://www.ptvdigitalarchive.org/" class="style183">Preserving Digital Public Television</a> <br>
              <br>
              </span><span class="style200">University of Illinois Urbana-Champaign</span><span class="style183"><br>
              <a href="http://www.ndiipp.uiuc.edu/" class="style187">The ECHODEPository<br>
              <br>
              </a></span><span class="style200">University of Michigan</span><br>
              <a href="http://www.icpsr.umich.edu/DATAPASS/" class="style183">Data Preservation Alliance for the Social Sciences</a></p>
          <p class="style183"><span class="style196">University of Maryland</span><br>
              <a href="http://www.dotcomarchive.org/" class="style183">DotComArchive</a></p>
          <p class="style183"><span class="style200">Emory University</span><br>
              <a href="http://www.metaarchive.org/" class="style184">MetaArchive</a></p>
          <p class="style183"><span class="style200">California Digital Library</span><br>
              <a href="http://www.cdlib.org/inside/projects/preservation/webatrisk/" class="style184">The Web at Risk</a></p>
          <p class="style174">&nbsp;</p>
          <div align="left">
            <div align="left" class="style1">
              <div align="left">
                <div align="left"></div>
              </div>
              </div>
        </div></td>
    </tr>
    <tr>
      <td height="128" colspan="5" valign="top"><img src="../images/Templates/bottom_title copy.jpg" width="550" height="38"></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p><br>
      <br>
      <br>
      <br>
  </p>
  <p>&nbsp;</p>
  <map name="Map3">
    <area shape="rect" coords="130,105,217,123" href="../participants.php">
    <area shape="rect" coords="29,106,75,124" href="../index.php">
    <area shape="rect" coords="273,108,410,124" href="../research.php">
    <area shape="rect" coords="583,105,682,123" href="../contact.php">
    <area shape="rect" coords="459,106,539,122" href="http://www.digitalpreservation.gov/">
  </map>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
