<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Collections Workshop II</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style164 {font-size: 12px}
.style227 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
body {
	margin-left: 5px;
	margin-top: 5px;
	background-image: url();
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #FFFFFF;
}
a {
	font-size: 12px;
	color: #FF0000;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
a:link {
	color: #000000;
	font-weight: bold;
	text-decoration: none;
}
a:visited {
	color: #333333;
	text-decoration: none;
}
a:hover {
	color: #CCCCCC;
	text-decoration: none;
}
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style225 {color: #000000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
a:active {
	text-decoration: none;
}
h1 {
	font-size: 10px;
	color: #0000FF;
}
h2 {
	font-size: 14px;
	color: #00FF33;
}
.style322 {font-family: "Times New Roman", Times, serif}
.style16 {color: #666666}
.style23 {font-size: 10px}
.style62 {font-size: 16px}
.style67 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000099;}
.style68 {color: #000000}
.style346 {
	font-size: 21px;
	color: #996600;
}
.style22 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
.style24 {font-size: 10}
.style64 {font-size: 16px; font-family: Arial, Helvetica, sans-serif; }
.style69 {font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #999999;}
.style72 {font-size: 14px; font-family: Arial, Helvetica, sans-serif; }
.style78 {font-family: Arial, Helvetica, sans-serif}
.style348 {color: #FFFFFF; font-family: Verdana, Arial, Helvetica, sans-serif; }
.style350 {
	font-size: 24px;
	color: #996600;
	font-weight: bold;
}
.style352 {color: #006633}
.style353 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function mmLoadMenus() {
  if (window.mm_menu_0307114853_0) return;
        window.mm_menu_0307114853_0 = new Menu("root",66,18,"Arial, Helvetica, sans-serif",12,"#000000","#CC9900","#C88804","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0307114853_0.addMenuItem("Dog","location='contact.php'");
  mm_menu_0307114853_0.addMenuItem("Cat","location='alliances.php'");
  mm_menu_0307114853_0.addMenuItem("Mouse","location='Advisory_Meeting_06.php'");
   mm_menu_0307114853_0.hideOnMouseOut=true;
   mm_menu_0307114853_0.bgColor='#555555';
   mm_menu_0307114853_0.menuBorder=1;
   mm_menu_0307114853_0.menuLiteBgColor='';
   mm_menu_0307114853_0.menuBorderBgColor='';
window.mm_menu_0307120033_0 = new Menu("root",91,18,"Arial, Helvetica, sans-serif",12,"#000000","#CC9900","#CC9900","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0307120033_0.addMenuItem("New&nbsp;item","location='contact.php'");
  mm_menu_0307120033_0.addMenuItem("New&nbsp;item2","location='Collection Reports.php'");
   mm_menu_0307120033_0.hideOnMouseOut=true;
   mm_menu_0307120033_0.bgColor='#555555';
   mm_menu_0307120033_0.menuBorder=1;
   mm_menu_0307120033_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0307120033_0.menuBorderBgColor='#777777';

                  window.mm_menu_0313104247_0 = new Menu("root",168,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313104247_0.addMenuItem(">Collections","location='Collection Reports.php'");
  mm_menu_0313104247_0.addMenuItem(">Technical&nbsp;Architecture","location='tech.php'");
   mm_menu_0313104247_0.hideOnMouseOut=true;
   mm_menu_0313104247_0.bgColor='#555555';
   mm_menu_0313104247_0.menuBorder=1;
   mm_menu_0313104247_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0313104247_0.menuBorderBgColor='#777777';
  window.mm_menu_0313111233_0 = new Menu("root",228,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313111233_0.addMenuItem("&nbsp;>UCSB","location='http://www.ucsb.edu'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Alexandria&nbsp;Digital&nbsp;Library","location='http://www.alexandria.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>UCSB&nbsp;Map&nbsp;&&nbsp;Imagery&nbsp;Lab","location='http://www.sdc.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>UCSB&nbsp;Libraries","location='http://www.library.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>Stanford","location='http://www.stanford.edu'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Stanford&nbsp;Libraries","location='http://www-sul.stanford.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Stanford&nbsp;Digital&nbsp;Repository","location='http://www-sul.stanford.edu/depts/pres/mediapres/digital.html'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>Library&nbsp;of&nbsp;Congress","location='http://www.loc.gov/index.html'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>NDIIPP","location='http://www.digitalpreservation.gov/'");
   mm_menu_0313111233_0.hideOnMouseOut=true;
   mm_menu_0313111233_0.bgColor='#555555';
   mm_menu_0313111233_0.menuBorder=0;
   mm_menu_0313111233_0.menuLiteBgColor='';
   mm_menu_0313111233_0.menuBorderBgColor='';
window.mm_menu_0313111538_0 = new Menu("root",229,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313111538_0.addMenuItem(">Collections&nbsp;2005","location='Collectionswrkshp05.htm'");
  mm_menu_0313111538_0.addMenuItem(">Advisory&nbsp;Board&nbsp;2006","location='Advisory_Meeting_06.php'");
  mm_menu_0313111538_0.addMenuItem(">Data&nbsp;Providers&nbsp;Workshop&nbsp;2005","location='March05.htm'");
   mm_menu_0313111538_0.hideOnMouseOut=true;
   mm_menu_0313111538_0.bgColor='#555555';
   mm_menu_0313111538_0.menuBorder=1;
   mm_menu_0313111538_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0313111538_0.menuBorderBgColor='#777777';

                                                  window.mm_menu_0315083721_0 = new Menu("root",216,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0315083721_0.addMenuItem("Advisory&nbsp;Board&nbsp;2006","location='Advisory_Meeting_06.php'");
  mm_menu_0315083721_0.addMenuItem("Data&nbsp;Providers&nbsp;workshop&nbsp;2005","location='March05.htm'");
  mm_menu_0315083721_0.addMenuItem("Collections&nbsp;Workshop&nbsp;2005","location='Collectionswrkshp05.htm'");
   mm_menu_0315083721_0.hideOnMouseOut=true;
   mm_menu_0315083721_0.bgColor='#555555';
   mm_menu_0315083721_0.menuBorder=1;
   mm_menu_0315083721_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0315083721_0.menuBorderBgColor='#777777';

  window.mm_menu_0315083721_0 = new Menu("root",173,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0315083721_0.addMenuItem("Technical&nbsp;Architecture","location='tech.php'");
  mm_menu_0315083721_0.addMenuItem("Collections","location='Collection Reports.php'");
  mm_menu_0315083721_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='Rights.php'");
   mm_menu_0315083721_0.hideOnMouseOut=true;
   mm_menu_0315083721_0.bgColor='#555555';
   mm_menu_0315083721_0.menuBorder=1;
   mm_menu_0315083721_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0315083721_0.menuBorderBgColor='#777777';

  window.mm_menu_0315132604_0 = new Menu("root",220,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0315132604_0.addMenuItem("Advisory&nbsp;Board&nbsp;Meeting&nbsp;2006","location='Advisory_Meeting_06.php'");
  mm_menu_0315132604_0.addMenuItem("Collections&nbsp;Workshop&nbsp;2005","location='Collectionswrkshp05.htm'");
  mm_menu_0315132604_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop&nbsp;2005","location='March05.htm'");
   mm_menu_0315132604_0.hideOnMouseOut=true;
   mm_menu_0315132604_0.bgColor='#555555';
   mm_menu_0315132604_0.menuBorder=1;
   mm_menu_0315132604_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0315132604_0.menuBorderBgColor='#777777';

  window.mm_menu_0315132805_0 = new Menu("root",207,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0315132805_0.addMenuItem("UCSB","location='http://www.ucsb.edu/'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;UCSB&nbsp;Libraries","location='http://www.library.ucsb.edu/'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;Alexandria&nbsp;Digital&nbsp;Library","location='http://www.alexandria.ucsb.edu/'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;UCSB&nbsp;Map&nbsp;&&nbsp;Imagery&nbsp;Lab","location='http://www.sdc.ucsb.edu/'");
  mm_menu_0315132805_0.addMenuItem("Stanford&nbsp;University","location='http://www.stanford.edu/'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;Stanford&nbsp;Libraries","location='http://www-sul.stanford.edu/'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;Stanford&nbsp;Digital&nbsp;Repository","location='http://www-sul.stanford.edu/depts/pres/mediapres/digital.html'");
  mm_menu_0315132805_0.addMenuItem("The&nbsp;Library&nbsp;of&nbsp;Congress","location='http://www.loc.gov/index.html'");
  mm_menu_0315132805_0.addMenuItem("&nbsp;&nbsp;&nbsp;NDIIPP","location='http://www.digitalpreservation.gov/'");
   mm_menu_0315132805_0.hideOnMouseOut=true;
   mm_menu_0315132805_0.bgColor='#555555';
   mm_menu_0315132805_0.menuBorder=1;
   mm_menu_0315132805_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0315132805_0.menuBorderBgColor='#777777';

            window.mm_menu_0319110541_0 = new Menu("root",304,18,"Arial, Helvetica, sans-serif",12,"#BDC4C4","#D29343","#D29343","#666666","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0319110541_0.addMenuItem("Geospatial&nbsp;Data&nbsp;Archiving&nbsp;&nbsp;NCSU","location='http://www.lib.ncsu.edu/ncgdap/index.html'");
  mm_menu_0319110541_0.addMenuItem("Data-PASS&nbsp;University&nbsp;if&nbsp;Michigan","location='http://www.icpsr.umich.edu/DATAPASS/'");
  mm_menu_0319110541_0.addMenuItem("ECHO&nbsp;DEPository&nbsp;Illinois&nbsp;Urbana/Champaign","location='http://www.ndiipp.uiuc.edu/'");
  mm_menu_0319110541_0.addMenuItem("Preserving&nbsp;Digital&nbsp;Public&nbsp;TV&nbsp;Thirteen/WNET","location='http://www.ptvdigitalarchive.org/'");
  mm_menu_0319110541_0.addMenuItem("DotcomArchive&nbsp;University&nbsp;of&nbsp;Maryland","location='http://www.dotcomarchive.org/'");
  mm_menu_0319110541_0.addMenuItem("MataArchive&nbsp;&nbsp;Emory&nbsp;University","location='http://www.metaarchive.org/'");
  mm_menu_0319110541_0.addMenuItem("The&nbsp;Web&nbsp;at&nbsp;Risk&nbsp;&nbsp;California&nbsp;Digital&nbsp;Library","location='http://www.cdlib.org/inside/projects/preservation/webatrisk/'");
   mm_menu_0319110541_0.hideOnMouseOut=true;
   mm_menu_0319110541_0.bgColor='#555555';
   mm_menu_0319110541_0.menuBorder=1;
   mm_menu_0319110541_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0319110541_0.menuBorderBgColor='#777777';
window.mm_menu_0319142013_0 = new Menu("root",406,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0319142013_0.addMenuItem("Data&nbsp;Provenance&nbsp;&nbsp;University&nbsp;of&nbsp;Arizona","location='http://kartik.eller.arizona.edu/'");
  mm_menu_0319142013_0.addMenuItem("Hopkins&nbsp;Storage&nbsp;Systems&nbsp;Lab&nbsp;&nbsp;Johns&nbsp;Hopkins","location='http://hssl.cs.jhu.edu/'");
  mm_menu_0319142013_0.addMenuItem("FOrmat&nbsp;CUration&nbsp;Service&nbsp;University&nbsp;of&nbsp;Maryland","location='http://www.umiacs.umd.edu/research/adapt/focus/index.html'");
  mm_menu_0319142013_0.addMenuItem("VidArch&nbsp;&nbsp;University&nbsp;of&nbsp;North&nbsp;Carolina","location='http://www.ils.unc.edu/vidarch/'");
  mm_menu_0319142013_0.addMenuItem("Logistical&nbsp;Computing&nbsp;and&nbsp;Internetworking&nbsp;&nbsp;&nbsp;U.&nbsp;Tennessee","location='http://loci.cs.utk.edu/'");
  mm_menu_0319142013_0.addMenuItem("Digital&nbsp;Preservation&nbsp;Lifecycle&nbsp;Management&nbsp;&nbsp;SD&nbsp;Supercomputer","location='http://www.sdsc.edu/srb/projects/digarch/main.html'");
  mm_menu_0319142013_0.addMenuItem("Geometric&nbsp;and&nbsp;Intelligent&nbsp;Computing&nbsp;Laboratory&nbsp;&nbsp;&nbsp;Drexel","location='http://gicl.cs.drexel.edu/wiki/Main_Page'");
  mm_menu_0319142013_0.addMenuItem("Incentives&nbsp;for&nbsp;Data&nbsp;Producers&nbsp;&nbsp;University&nbsp;of&nbsp;Michigan","location='http://www.si.umich.edu/incentives/'");
   mm_menu_0319142013_0.hideOnMouseOut=true;
   mm_menu_0319142013_0.bgColor='#555555';
   mm_menu_0319142013_0.menuBorder=1;
   mm_menu_0319142013_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0319142013_0.menuBorderBgColor='#777777';

mm_menu_0319142013_0.writeMenus();
} // mmLoadMenus()


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_jumpMenuGo(selName,targ,restore){ //v3.0
  var selObj = MM_findObj(selName); if (selObj) MM_jumpMenu(targ,selObj,restore);
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript" src="../js/mm_menu.js"></script>
</head>
<body onLoad="MM_preloadImages('../images/Interfaces/Interface1.jpg','../images/index/SideBar/MeetingsBlack.jpg','../images/index/SideBar/ResearchBlack.jpg','../images/index/SideBar/ParticipantsBlack.jpg','../images/index/SideBar/ContactBlack.jpg','../images/Interfaces/Format RegistrySwap.jpg','../images/Interfaces/Interface1 copy 4.jpg','../images/index/SideBar/HomeBlack.jpg','../images/index/SideBar/2005Grey.jpg','../images/Interfaces/InterfaceInfoSwap.jpg')">
<script language="JavaScript1.2">mmLoadMenus();</script> 
<table width="760"  border="0" cellspacing="0" cellpadding="0"> 
  <tr> 
    <td height="214" colspan="4" valign="top"><div align="center"> 
        <p><img src="../images/templates/0223Banner.jpg" width="760" height="211" border="0"><br> 
        </p> 
      </div></td> 
  </tr> 
  <tr> 
    <td width="20%" height="1123" rowspan="5" valign="top" background="../images/PhaseIIImages/Backgrounds/MenuBG.jpg"><div align="left" class="style225"> 
        <p class="style24">&nbsp;</p>
        <table width="165" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><span class="style24"><span class="style164"><img src="../images/index/SideBar/NGDA.jpg" width="166" height="19"></span></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><span class="style24"><span class="style164"><a href="../index.php" onMouseOver="MM_swapImage('Home','','../images/index/SideBar/HomeBlack.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/index/SideBar/Home.jpg" alt="Home" name="Home" width="166" height="19" border="0" id="Home"></a></span></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><span class="style24"><span class="style164"><a href="javascript:;" onMouseOver="MM_swapImage('Image1','','../images/index/SideBar/ResearchBlack.jpg',1);MM_showMenu(window.mm_menu_0315083721_0,163,16,null,'Image1')" onMouseOut="MM_swapImgRestore();MM_startTimeout();"><img src="../images/index/SideBar/Research.jpg" alt="Research" name="Image1" width="166" height="19" border="0" id="Image1"></a></span></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><span class="style24"><span class="style164"><a href="javascript:;" onMouseOver="MM_swapImage('Image2','','../images/index/SideBar/MeetingsBlack.jpg',1);MM_showMenu(window.mm_menu_0315132604_0,163,16,null,'Image2')" onMouseOut="MM_swapImgRestore();MM_startTimeout();"><img src="../images/index/SideBar/Meetings.jpg" alt="Meetings" name="Image2" width="166" height="19" border="0" id="Image2"></a></span></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="javascript:;" onMouseOver="MM_swapImage('Image3','','../images/index/SideBar/ParticipantsBlack.jpg',1);MM_showMenu(window.mm_menu_0315132805_0,163,16,null,'Image3')" onMouseOut="MM_swapImgRestore();MM_startTimeout();"><img src="../images/index/SideBar/Participants.jpg" alt="Participants" name="Image3" width="166" height="19" border="0" id="Image3"></a></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><a href="../contact.php" onMouseOver="MM_swapImage('Image4','','../images/index/SideBar/ContactBlack.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/index/SideBar/Contact.jpg" alt="Contact NGDA" name="Image4" width="166" height="19" border="0" id="Image4"></a></td>
          </tr>
        </table>
        </div> 
      <p>&nbsp;</p>
      <table width="165" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/index/SideBar/NDIIPP.jpg" width="166" height="19"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><a href="javascript:;" onMouseOver="MM_showMenu(window.mm_menu_0319142013_0,163,16,null,'image3');MM_swapImage('image3','','../images/index/SideBar/2005Grey.jpg',1)" onMouseOut="MM_startTimeout();MM_swapImgRestore()"><img src="../images/index/SideBar/2005Brown.jpg" alt="NDIIPP 2005 Awards" name="image3" width="166" height="19" border="0" id="image3"></a></td>
        </tr>
      </table>      <p>&nbsp;</p>
      <br> 
      <p align="left" class="style227"><strong><span class="style348">&nbsp;&gt;<span class="style164">&nbsp;Contributing &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Institutions</span></span> </strong></p> 
      <form name="form1" method="post" action=""> 
        <p align="left"> 
          <select name="select5" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>UCSB &amp; Stanford</option> 
            <option value="http://www.lockss.org/lockss/Home">SU - LOCKSS Project</option> 
            <option value="http://www.ncgia.ucsb.edu/">UCSB - NCGIA Proj.</option> 
            <option value="http://eil.bren.ucsb.edu/">UCSB - Env. Info. Lab</option> 
            <option value="http://db.cosmos-eq.org/scripts/default.plx">UCSB - COSMOS</option> 
          </select> 
        </p> 
      </form> 
      <form name="form2" method="post" action=""> 
        <p align="left"> 
          <select name="select6" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>Govt. Agencies</option> 
            <option value="http://www.usgs.gov/">USGS</option> 
            <option value="http://gis.ca.gov/">CaSIL</option> 
            <option value="http://www.ceres.ca.gov/">California Resources</option> 
            <option value="http://www.loc.gov/index.html">Library of Congress</option> 
            <option value="http://www.digitalpreservation.gov/index.html">NDIIPP</option> 
            <option value="http://www.ss.ca.gov/archives/archives_e.htm">CA State Archives</option> 
          </select> 
        </p> 
      </form> 
      <p align="left"> 
        <select name="select4" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
          <option selected>Academic Institutions</option> 
          <option value="http://www.umiacs.umd.edu/index.htm">U. Md - UMIACS</option> 
          <option value="http://www.princeton.edu/~geolib/gis/">Princeton - GIS</option> 
          <option value="http://content.lib.washington.edu/contact.html">Univ of Washington</option> 
          <option value="http://www.lib.ncsu.edu/dli/">NCSU - DLI</option> 
          <option value="http://www.library.uiuc.edu/max/">Un. Illinois, Urb-Ch</option> 
          <option value="http://www.ucdavis.edu/index.html">UC - Davis</option> 
          <option value="http://www.sdsc.edu/">SD Supercomputer</option> 
          <option value="http://map.sdsu.edu/visual/">SDSU - Visual. Ctr</option> 
          <option value="http://www.nypl.org/">NY Public Library</option> 
        </select> 
        <br> 
        <br> 
        <select name="select7" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
          <option selected>Private &amp; Industry</option> 
          <option value="http://www.davidrumsey.com/">Rumsey Cartography </option> 
          <option value="http://www.archivas.com/">Archivas</option> 
          <option value="http://www.esri.com/">ESRI</option> 
          <option value="http://www.isilon.com/">Isilon</option> 
          <option value="http://www.sun.com/">Sun Microsystems</option> 
        </select> 
      </p> 
      <p align="left">&nbsp; </p> 
      <p align="left">&nbsp;</p> 
      <div align="left" class="style22"> 
        <p class="style23">&nbsp;</p> 
    </div></td> 
    <td width="1%" rowspan="5"><p class="style227">&nbsp;</p> 
    <p>&nbsp;</p></td> 
    <td colspan="2" valign="top"><p align="left" class="style346"><span class="style350">NGDA </span></p> 
      <p align="left" class="style78">From the first colonial maps to the satellite imagery of the 21st century, cartographic information has helped define and frame our view of the United States.<br> 
        Geospatial imagery is now a critical tool for state and federal researchers pursuing greater understandings of complex matters including: Disaster Relief, Census Data, and Land Use Management <br> 
      </p>      <p align="left" class="style78">Concerned that millions of nationally important digital information resources are in danger of being lost or corrupted, the Library of Congress has formed The National Digital Information Infrastructure and Preservation Program (NDIIPP).</p>
      <p align="left" class="style78">&nbsp;</p>      </td> 
  </tr>
  <tr>
    <td width="49%" valign="top"><p class="style78">As partners in the project the university libraries of UCSB and Stanford are leading the formation of the National Geospatial Digital Archive (NGDA), a collecting network for the archiving of geospatial images and data. </p>
      <p class="style78">&nbsp;</p>
      <p class="style78"><span class="style24">They have partnered with eight institutions to begin a three year, $15 million effort to begin construction of a nationwide digital collection and preservation system.</span><br>
      </p>
      <p class="style78">The objectives of the project are to: <br>
        <br>
      </p></td>
    <td width="30%" valign="top">
      <div align="right"><span class="style346"><span class="style78"><a href="http://clients.alexandria.ucsb.edu/ngda/" onMouseOver="MM_swapImage('image11','','../images/Interfaces/Interface1 copy 4.jpg',1)" onMouseOut="MM_swapImgRestore()"><br>
    </a></span></span></div></td></tr>
  <tr>
    <td colspan="2" valign="top"><div align="left">
        <div align="left">
          <ul>
            <li class="style78">
              <p>Create a new national federated network committed to archiving geospatial<br>
            imagery and data. </p>
            <li class="style78">
              <p>Investigate the proper and optimal roles of such a federated <br>
          archive, with consideration of distant (dark) backup and migration, directly serving <br>
          content to users, vs. referring requestors back to the originators of the data for copies or assistance, active or passive quality/integrity monitoring, application of metadata, <br>
          federated searching, dissemination of metadata, etc. </p>
            <li class="style78">
              <p>Collect and archive major segments of at-risk digital geospatial data and images. </p>
            <li class="style78">
              <div align="left">
                <p>Develop best practices for the presentation of archived digital geospatial data.</p>
              </div>
            </li>
            <li class="style78">
              <div align="left">
                <div align="left">
                  <p>Develop partner communication mechanisms for the project and then ongoing. <br>
                  </p>
                </div>
              </div>
            </li>
            <li>
              <div align="left">
                <div align="left">
                  <p class="style78">Develop a series of policy agreements governing retention, rights management, <br>
              obligations of partners, interoperability of systems, and exchange of digital objects.</p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <p align="left"><span class="style24"><span class="style346"><span class="style78"><a href="http://clients.alexandria.ucsb.edu/ngda/" onMouseOver="MM_swapImage('image11','','../images/Interfaces/Interface1 copy 4.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/Interfaces/Interface1Beta.jpg" alt="NGDA Interface" name="image11" width="154" height="48" border="0" id="image1"></a><a href="http://clients.alexandria.ucsb.edu/ngda/help.html" onMouseOver="MM_swapImage('Image6','','../images/Interfaces/InterfaceInfoSwap.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/Interfaces/CB_Help.jpg" name="Image6" width="154" height="48" border="0" id="Image6"></a><span class="style225"><a href="http://ngda.library.ucsb.edu/format" onMouseOver="MM_swapImage('Image5','','../images/Interfaces/Format RegistrySwap.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/Interfaces/Format Registry.jpg" alt="Format Registry" name="Image5" width="154" height="48" border="0" id="Image5"></a></span></span></span></span></p>
      <div align="left">
        <div align="left">
          <div align="left">
            <div align="left"><br>
        </div>
          </div>
        </div>
      </div>
      <span class="style24"><span class="style346"></span></span></td>
  </tr>
  <tr>
    <td colspan="2" valign="top"><span class="style64"><span class="style67"><span class="style16"><span class="style68"><img src="../images/stanford_S.jpg" width="68" height="68"><img src="../Templates/sm-gaucho-logo copy.jpg" width="100" height="66"></span></span></span></span></td>
  </tr>
  <tr>
    <td colspan="2" valign="top"><span class="style64"><img src="../images/Templates/bottom_title copy.jpg" width="550" height="38"></span></td>
  </tr> 
  <tr> 
    <td colspan="4" align="center" valign="top">&nbsp;</td> 
  </tr> 
  <tr> 
    <td align="center" valign="top"><div align="left"><span class="style164 style322 style23">Copyright &copy; 2005-2007 <br> 
        University of California,<br> 
        Santa Barbara, CA 93106 <br> 
        (805) 893-8000</span></div></td> 
    <td align="center" valign="top">&nbsp;</td> 
    <td colspan="2" align="center" valign="top"><div align="right"><span class="style164 style322 style23">Last Modified<br> 
        March 02, 2007 </span></div></td> 
  </tr> 
</table> 
<p>&nbsp;</p> 
<p>&nbsp; </p> 
<map name="Map"> 
  <area shape="rect" coords="4,61,68,80" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313104247_0,68,61,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="5,130,90,147" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313111233_0,90,130,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="3,98,63,112" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313111538_0,63,98,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="3,32,47,49" href="#" onClick="MM_jumpMenuGo('select8','parent',0)"> 
  <area shape="rect" coords="3,166,62,183" href="#"> 
</map> 
</body>
</html>
