<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Collections Workshop II</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style164 {font-size: 12px}
.style227 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
body {
	margin-left: 5px;
	margin-top: 5px;
	background-image: url();
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #FFFFFF;
}
a {
	font-size: 12px;
	color: #FF0000;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
a:link {
	color: #000000;
	font-weight: bold;
	text-decoration: none;
}
a:visited {
	color: #333333;
	text-decoration: none;
}
a:hover {
	color: #CCCCCC;
	text-decoration: none;
}
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style225 {color: #000000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
a:active {
	text-decoration: none;
}
h1 {
	font-size: 10px;
	color: #0000FF;
}
h2 {
	font-size: 14px;
	color: #00FF33;
}
.style322 {font-family: "Times New Roman", Times, serif}
.style326 {color: #FFFFFF}
.style16 {color: #666666}
.style23 {font-size: 10px}
.style62 {font-size: 16px}
.style67 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000099;}
.style68 {color: #000000}
.style346 {
	font-size: 21px;
	color: #996600;
}
.style22 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
.style24 {font-size: 10}
.style64 {font-size: 16px; font-family: Arial, Helvetica, sans-serif; }
.style69 {font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #999999;}
.style72 {font-size: 14px; font-family: Arial, Helvetica, sans-serif; }
.style78 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function mmLoadMenus() {
  if (window.mm_menu_0307114853_0) return;
        window.mm_menu_0307114853_0 = new Menu("root",66,18,"Arial, Helvetica, sans-serif",12,"#000000","#CC9900","#C88804","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0307114853_0.addMenuItem("Dog","location='contact.php'");
  mm_menu_0307114853_0.addMenuItem("Cat","location='alliances.php'");
  mm_menu_0307114853_0.addMenuItem("Mouse","location='Advisory_Meeting_06.php'");
   mm_menu_0307114853_0.hideOnMouseOut=true;
   mm_menu_0307114853_0.bgColor='#555555';
   mm_menu_0307114853_0.menuBorder=1;
   mm_menu_0307114853_0.menuLiteBgColor='';
   mm_menu_0307114853_0.menuBorderBgColor='';
window.mm_menu_0307120033_0 = new Menu("root",91,18,"Arial, Helvetica, sans-serif",12,"#000000","#CC9900","#CC9900","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0307120033_0.addMenuItem("New&nbsp;item","location='contact.php'");
  mm_menu_0307120033_0.addMenuItem("New&nbsp;item2","location='Collection Reports.php'");
   mm_menu_0307120033_0.hideOnMouseOut=true;
   mm_menu_0307120033_0.bgColor='#555555';
   mm_menu_0307120033_0.menuBorder=1;
   mm_menu_0307120033_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0307120033_0.menuBorderBgColor='#777777';

                  window.mm_menu_0313104247_0 = new Menu("root",168,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313104247_0.addMenuItem(">Collections","location='Collection Reports.php'");
  mm_menu_0313104247_0.addMenuItem(">Technical&nbsp;Architecture","location='tech.php'");
   mm_menu_0313104247_0.hideOnMouseOut=true;
   mm_menu_0313104247_0.bgColor='#555555';
   mm_menu_0313104247_0.menuBorder=1;
   mm_menu_0313104247_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0313104247_0.menuBorderBgColor='#777777';
  window.mm_menu_0313111233_0 = new Menu("root",228,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313111233_0.addMenuItem("&nbsp;>UCSB","location='http://www.ucsb.edu'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Alexandria&nbsp;Digital&nbsp;Library","location='http://www.alexandria.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>UCSB&nbsp;Map&nbsp;&&nbsp;Imagery&nbsp;Lab","location='http://www.sdc.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>UCSB&nbsp;Libraries","location='http://www.library.ucsb.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>Stanford","location='http://www.stanford.edu'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Stanford&nbsp;Libraries","location='http://www-sul.stanford.edu/'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>Stanford&nbsp;Digital&nbsp;Repository","location='http://www-sul.stanford.edu/depts/pres/mediapres/digital.html'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>Library&nbsp;of&nbsp;Congress","location='http://www.loc.gov/index.html'");
  mm_menu_0313111233_0.addMenuItem("&nbsp;>NDIIPP","location='http://www.digitalpreservation.gov/'");
   mm_menu_0313111233_0.hideOnMouseOut=true;
   mm_menu_0313111233_0.bgColor='#555555';
   mm_menu_0313111233_0.menuBorder=0;
   mm_menu_0313111233_0.menuLiteBgColor='';
   mm_menu_0313111233_0.menuBorderBgColor='';
window.mm_menu_0313111538_0 = new Menu("root",229,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0313111538_0.addMenuItem(">Collections&nbsp;2005","location='Collectionswrkshp05.htm'");
  mm_menu_0313111538_0.addMenuItem(">Advisory&nbsp;Board&nbsp;2006","location='Advisory_Meeting_06.php'");
  mm_menu_0313111538_0.addMenuItem(">Data&nbsp;Providers&nbsp;Workshop&nbsp;2005","location='March05.htm'");
   mm_menu_0313111538_0.hideOnMouseOut=true;
   mm_menu_0313111538_0.bgColor='#555555';
   mm_menu_0313111538_0.menuBorder=1;
   mm_menu_0313111538_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0313111538_0.menuBorderBgColor='#777777';

              window.mm_menu_0315083721_0 = new Menu("root",166,18,"Arial, Helvetica, sans-serif",12,"#000000","#D29343","#D29343","#000000","left","middle",3,0,500,-5,7,true,false,true,0,false,true);
  mm_menu_0315083721_0.addMenuItem("New&nbsp;item","location='Advisory_Meeting_06.php'");
  mm_menu_0315083721_0.addMenuItem("New&nbsp;item2","location='Copy of NGDAhome.php'");
  mm_menu_0315083721_0.addMenuItem("New&nbsp;item3","location='contributors.php'");
   mm_menu_0315083721_0.hideOnMouseOut=true;
   mm_menu_0315083721_0.bgColor='#555555';
   mm_menu_0315083721_0.menuBorder=1;
   mm_menu_0315083721_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0315083721_0.menuBorderBgColor='#777777';

mm_menu_0315083721_0.writeMenus();
} // mmLoadMenus()


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_jumpMenuGo(selName,targ,restore){ //v3.0
  var selObj = MM_findObj(selName); if (selObj) MM_jumpMenu(targ,selObj,restore);
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript" src="../js/mm_menu.js"></script>
</head>
<body onLoad="MM_preloadImages('../images/Interfaces/Interface1.jpg','../images/index/SideBar/HomeBlack.jpg')"> 
<script language="JavaScript1.2">mmLoadMenus();</script> 
<table width="760"  border="0" cellspacing="0" cellpadding="0"> 
  <tr> 
    <td height="214" colspan="3" valign="top"><div align="center"> 
        <p align="left"><img src="../images/templates/0223Banner.jpg" width="760" height="211" border="0"><br> 
        </p> 
      </div></td> 
  </tr> 
  <tr> 
    <td width="22%" valign="top" background="../images/PhaseIIImages/Backgrounds/MenuBG.jpg"><div align="left" class="style225"> 
        <p class="style24">&nbsp;</p>
        <p class="style24"><strong> </strong></p>
        <p><span class="style164"><a href="javascript:;" onMouseOver="MM_showMenu(window.mm_menu_0315083721_0,166,0,null,'image3');MM_swapImage('image3','','../images/index/SideBar/HomeBlack.jpg',1)" onMouseOut="MM_startTimeout();"><img src="file:///N|/images/index/SideBar/Home.jpg" name="image3" width="166" height="19" border="0" id="image3"></a><br> 
          <br> 
          </span></p> 
      </div> 
      <table width="154" height="135" border="0" cellpadding="0" cellspacing="0"> 
        <tr> 
          <td width="154" height="48" valign="top"><a href="http://clients.alexandria.ucsb.edu/ngda/" onClick="MM_popupMsg('I\'m Here')" onMouseOver="MM_showMenu(window.mm_menu_0307114853_0,154,0,null,'image1')" onMouseOut="MM_startTimeout();"><img src="../images/Interfaces/Interface1Beta.jpg" name="image1" width="154" height="48" border="0" id="image1"></a></td> 
        </tr> 
        <tr> 
          <td height="21" valign="top">&nbsp;</td> 
        </tr> 
        <tr> 
          <td height="66" valign="top"><p><a href="http://clients.alexandria.ucsb.edu/ngda/help.html"><img src="../images/Interfaces/CB_Help.jpg" alt="Interface Help" width="154" height="48" border="0"></a></p> 
            <p><a href="http://ngda.library.ucsb.edu/format"><img src="../images/Interfaces/Format Registry.jpg" alt="Format Registry" width="154" height="48" border="0"></a></p></td> 
        </tr> 
      </table> 
      <form name="form1" method="post" action=""> 
        <p align="left" class="style61 style75 style164 style326"><strong>&nbsp;&gt;NGDA and NDIIPP</strong></p> 
        <p><br>
        </p>
        <table width="100" border="0" cellpadding="0" cellspacing="0" bgcolor="#D29343">
          <tr>
            <td><a href="../alliances.php">&nbsp;&gt;Cat</a> </td>
          </tr>
        </table>
        <p>&nbsp;        </p>
        <p align="left" class="style61 style75 style164 style326"><strong>      </strong></p>
        <p align="left"> 
          <select name="select8" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option>NGDA Participants</option> 
            <option value="http://www.ucsb.edu/">UC Santa Barbara</option> 
            <option value="http://www.library.ucsb.edu/" selected>UCSB Libraries</option> 
            <option value="http://www.alexandria.ucsb.edu/">Alexandria Dig. Lib.</option> 
            <option value="http://www.sdc.ucsb.edu/">UCSB Map &amp; Img Lab</option> 
            <option value="http://www.stanford.edu/">Stanford University</option> 
            <option value="http://www-sul.stanford.edu/">Stanford Libraries</option> 
            <option value="http://www-sul.stanford.edu/depts/pres/mediapres/digital.html">SU Dig. Repository</option> 
            <option value="http://www.digitalpreservation.gov/index.html">NDIIPP</option> 
          </select> 
        </p> 
      </form> 
      <form name="form2" method="post" action=""> 
        <p align="left"> 
          <select name="select2" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>NDIIPP Projects</option> 
            <option value="http://www.lib.ncsu.edu/ncgdap/index.html">North Carolina State</option> 
            <option value="http://www.icpsr.umich.edu/DATAPASS/">University of Michig.</option> 
            <option value="http://www.ndiipp.uiuc.edu/">University of Illinois</option> 
            <option value="http://www.ptvdigitalarchive.org/">Ed Broadcasting/wnet</option> 
            <option value="http://www.dotcomarchive.org/">University of Mrylnd</option> 
            <option value="http://www.metaarchive.org/">Emory University</option> 
            <option value="http://www.cdlib.org/inside/projects/preservation/webatrisk/">Ca. Dig. Library</option> 
          </select> 
        </p> 
        <p align="left"> 
          <select name="select" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>NDIIPP 2005 Awards</option> 
            <option value="http://kartik.eller.arizona.edu/">Univ. of Arizona</option> 
            <option value="http://hssl.cs.jhu.edu/">Johns Hopkins</option> 
            <option value="http://www.umiacs.umd.edu/research/adapt/focus/index.html">Univ. of Maryland</option> 
            <option value="http://www.ils.unc.edu/vidarch/">Univ.of North Carolina</option> 
            <option value="http://www.cs.odu.edu/%7Ejsmit/digarch/">Old Dominion</option> 
            <option value="http://loci.cs.utk.edu/">Univ. Tennessee</option> 
            <option value="http://gdc.ucsd.edu:8080/digarch">Scripps Inst. Ocean.</option> 
            <option value="http://www.sdsc.edu/srb/projects/digarch/main.html">SD Supercomputer</option> 
            <option value="http://gicl.cs.drexel.edu/wiki/Main_Page">Drexel University</option> 
            <option value="http://www.si.umich.edu/incentives/">Univ. Michigan</option> 
          </select> 
        </p> 
      </form> 
      <br> 
      <p align="left" class="style227"><strong><span class="style326">&gt;Contributing Institutions</span> </strong></p> 
      <form name="form1" method="post" action=""> 
        <p align="left"> 
          <select name="select5" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>UCSB &amp; Stanford</option> 
            <option value="http://www.lockss.org/lockss/Home">SU - LOCKSS Project</option> 
            <option value="http://www.ncgia.ucsb.edu/">UCSB - NCGIA Proj.</option> 
            <option value="http://eil.bren.ucsb.edu/">UCSB - Env. Info. Lab</option> 
            <option value="http://db.cosmos-eq.org/scripts/default.plx">UCSB - COSMOS</option> 
          </select> 
        </p> 
      </form> 
      <form name="form2" method="post" action=""> 
        <p align="left"> 
          <select name="select6" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
            <option selected>Govt. Agencies</option> 
            <option value="http://www.usgs.gov/">USGS</option> 
            <option value="http://gis.ca.gov/">CaSIL</option> 
            <option value="http://www.ceres.ca.gov/">California Resources</option> 
            <option value="http://www.loc.gov/index.html">Library of Congress</option> 
            <option value="http://www.digitalpreservation.gov/index.html">NDIIPP</option> 
            <option value="http://www.ss.ca.gov/archives/archives_e.htm">CA State Archives</option> 
          </select> 
        </p> 
      </form> 
      <p align="left"> 
        <select name="select4" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
          <option selected>Academic Institutions</option> 
          <option value="http://www.umiacs.umd.edu/index.htm">U. Md - UMIACS</option> 
          <option value="http://www.princeton.edu/~geolib/gis/">Princeton - GIS</option> 
          <option value="http://content.lib.washington.edu/contact.html">Univ of Washington</option> 
          <option value="http://www.lib.ncsu.edu/dli/">NCSU - DLI</option> 
          <option value="http://www.library.uiuc.edu/max/">Un. Illinois, Urb-Ch</option> 
          <option value="http://www.ucdavis.edu/index.html">UC - Davis</option> 
          <option value="http://www.sdsc.edu/">SD Supercomputer</option> 
          <option value="http://map.sdsu.edu/visual/">SDSU - Visual. Ctr</option> 
          <option value="http://www.nypl.org/">NY Public Library</option> 
        </select> 
        <br> 
        <br> 
        <select name="select7" class="style227" onChange="MM_jumpMenu('parent',this,0)"> 
          <option selected>Private &amp; Industry</option> 
          <option value="http://www.davidrumsey.com/">Rumsey Cartography </option> 
          <option value="http://www.archivas.com/">Archivas</option> 
          <option value="http://www.esri.com/">ESRI</option> 
          <option value="http://www.isilon.com/">Isilon</option> 
          <option value="http://www.sun.com/">Sun Microsystems</option> 
        </select> 
      </p> 
      <p align="left">&nbsp; </p> 
      <p align="left">&nbsp;</p> 
      <div align="left" class="style22"> 
        <p class="style23">&nbsp;</p> 
      </div></td> 
    <td width="1%"><p class="style227">&nbsp;</p> 
      <p>&nbsp;</p></td> 
    <td width="77%" valign="top"><p class="style346"><strong>NGDA </strong></p> 
      <p align="left">&nbsp; </p> 
      <p align="left" class="style78">From the first colonial maps to the satellite imagery of the 21st century, cartographic information has helped define and frame our view of the United States.<br> 
        Geospatial imagery is now a critical tool for state and federal researchers pursuing greater understandings of complex matters including: Disaster Relief, Census Data, and Land Use Management <br> 
        <br> 
        Concerned that millions of nationally important digital information resources are in danger of being lost or corrupted, the Library of Congress has formed The National Digital Information Infrastructure and Preservation Program (NDIIPP).<br> 
        <br> 
        As partners in the project the university libraries of UCSB and Stanford are leading the formation of the National Geospatial Digital Archive (NGDA), a collecting network for the archiving of geospatial images and data. They have partnered with eight institutions to begin a three year, $15 million effort to begin construction of a nationwide digital collection and preservation system.</p> 
      <p align="left" class="style78">&nbsp;</p> 
      <p align="left" class="style78">The objectives of the project are to: </p> 
      <div align="left"> 
        <div align="left"> 
          <ul> 
            <li class="style78"> 
              <p>Create a new national federated network committed to archiving geospatial<br> 
                imagery and data. </p> 
            <li class="style78"> 
              <p>Investigate the proper and optimal roles of such a federated <br> <?php if (@$_GET['page_']) include $_GET['page_']; ?>
                archive, with consideration of distant (dark) backup and migration, directly serving <br> 
                content to users, vs. referring requestors back to the originators of the data for copies or assistance, active or passive quality/integrity monitoring, application of metadata, <br> 
                federated searching, dissemination of metadata, etc. </p> 
            <li class="style78"> 
              <p>Collect and archive major segments of at-risk digital geospatial data and images. </p> 
            <li class="style78"> 
              <div align="left"> 
                <p>Develop best practices for the presentation of archived digital geospatial data.</p> 
              </div> 
            </li> 
            <li class="style78"> 
              <div align="left"> 
                <div align="left"> 
                  <p>Develop partner communication mechanisms for the project and then ongoing. <br> 
                  </p> 
                </div> 
              </div> 
            </li> 
            <li> 
              <div align="left"> 
                <div align="left"> 
                  <p class="style78">Develop a series of policy agreements governing retention, rights management, <br> 
                    obligations of partners, interoperability of systems, and exchange of digital objects.</p> 
                </div> 
              </div> 
            </li> 
          </ul> 
        </div> 
      </div> 
      <p align="left">&nbsp;</p> 
      <div align="left"> 
        <div align="left"> 
          <div align="left"> 
            <div align="left"><span class="style72"></span></div> 
          </div> 
          <ul> 
            <span class="style22"></span> 
          </ul> 
        </div> 
        <span class="style62"></span></div> 
      <p class="style64"><span class="style67"><span class="style16"><span class="style68"><span class="style69"> &nbsp;</span>&nbsp; <img src="../Templates/sm-gaucho-logo copy.jpg" width="100" height="66"><img src="../images/stanford_S.jpg" width="68" height="68"></span></span></span><br> 
        <img src="../images/Templates/bottom_title copy.jpg" width="550" height="38"><br> 
      </p></td> 
  </tr> 
  <tr> 
    <td colspan="3" align="center" valign="top">&nbsp;</td> 
  </tr> 
  <tr> 
    <td align="center" valign="top"><div align="left"><span class="style164 style322 style23">Copyright &copy; 2005-2007 <br> 
        University of California,<br> 
        Santa Barbara, CA 93106 <br> 
        (805) 893-8000</span></div></td> 
    <td align="center" valign="top">&nbsp;</td> 
    <td align="center" valign="top"><div align="right"><span class="style164 style322 style23">Last Modified<br> 
        March 02, 2007 </span></div></td> 
  </tr> 
</table> 
<p>&nbsp;</p> 
<p>&nbsp; </p> 
<map name="Map"> 
  <area shape="rect" coords="4,61,68,80" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313104247_0,68,61,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="5,130,90,147" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313111233_0,90,130,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="3,98,63,112" href="#" onMouseOver="MM_showMenu(window.mm_menu_0313111538_0,63,98,null,'image2')" onMouseOut="MM_startTimeout();"> 
  <area shape="rect" coords="3,32,47,49" href="#" onClick="MM_jumpMenuGo('select8','parent',0)"> 
  <area shape="rect" coords="3,166,62,183" href="#"> 
</map> 
</body>
</html>
