<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Map&amp;Imagery</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style3 {color: #FFFFFF}
.style8 {
	font-size: 12px;
	color: #000000;
}
.style9 {color: #FFFFFF; font-size: 12px; }
a:link {
	color: #000099;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #336699;
}
a:hover {
	text-decoration: none;
	color: #666666;
}
a:active {
	text-decoration: none;
}
.style11 {color: #000000}
.style12 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.style15 {font-family: Arial, Helvetica, sans-serif}
.style19 {
	font-size: 12px;
	color: #000000;
	font-style: italic;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style33 {
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
	color: #FFFFFF;
}
.style34 {font-size: 11px}
.style46 {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 12px;
}
.style51 {font-size: 16; font-family: "Times New Roman", Times, serif;}
.style59 {font-size: 16px; font-family: "Times New Roman", Times, serif; }
.style63 {font-family: Arial, Helvetica, sans-serif; color: #000000; }
.style66 {font-size: 12px; color: #000000; font-weight: bold; }
.style76 {color: #000066; font-size: 18px; font-family: Arial, Helvetica, sans-serif; }
.style78 {font-size: 12px}
.style80 {color: #000066}
.style82 {color: #000066; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.style92 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 12px;}
.style94 {font-family: Georgia, "Times New Roman", Times, serif; font-size: 12px; font-style: italic; }
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666666;
	font-weight: bold;
}
.style95 {font-size: 18px; font-weight: bold;}
.style96 {color: #FFFFFF; font-weight: bold; font-size: 12px; }
.style97 {
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style98 {font-style: italic; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 12px;}
.style99 {font-weight: bold; font-size: 12px;}
.style100 {font-size: 10px}
.style101 {font-size: 10px; color: #FFFFFF; }
.style102 {font-size: 10px; color: #000000; }
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--

function mmLoadMenus() {
  if (window.mm_menu_0318111657_0) return;
  window.mm_menu_0318111657_0 = new Menu("root",230,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#333333","#333333","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0318111657_0.addMenuItem("Pegasus:&nbsp;On-Line&nbsp;Catalog","location='http://pegasus-16.library.ucsb.edu/F/?func=find-b-0'");
  mm_menu_0318111657_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0318111657_0.addMenuItem("Air&nbsp;Photos:&nbsp;Ready&nbsp;Reference&nbsp;Aids","location='http://www.sdc.ucsb.edu/holdings/ready-ref/'");
   mm_menu_0318111657_0.hideOnMouseOut=true;
   mm_menu_0318111657_0.bgColor='#555555';
   mm_menu_0318111657_0.menuBorder=1;
   mm_menu_0318111657_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0318111657_0.menuBorderBgColor='#777777';

            window.mm_menu_0318112034_0 = new Menu("root",195,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0318112034_0.addMenuItem("Pegasus&nbsp;On-Line&nbsp;Catalog","location='http://pegasus-16.library.ucsb.edu/F/?func=find-b-0'");
  mm_menu_0318112034_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0318112034_0.addMenuItem("Air&nbsp;Photos&nbsp;Ready&nbsp;Reference","location='http://www.sdc.ucsb.edu/holdings/ready-ref/'");
   mm_menu_0318112034_0.hideOnMouseOut=true;
   mm_menu_0318112034_0.bgColor='#CCCCCC';
   mm_menu_0318112034_0.menuBorder=1;
   mm_menu_0318112034_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0318112034_0.menuBorderBgColor='#333333';

                    window.mm_menu_0318112923_0 = new Menu("root",182,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0318112923_0.addMenuItem("By&nbsp;County","location='http://webtest.library.ucsb.edu/ap_indexes/'");
  mm_menu_0318112923_0.addMenuItem("By&nbsp;Flight&nbsp;ID","location='http://collections.alexandria.ucsb.edu/ap/indexes/'");
  mm_menu_0318112923_0.addMenuItem("Air&nbsp;Photo&nbsp;Scanning&nbsp;Status","location='http://www.sdc.ucsb.edu/projects/scan_progress_table.html'");
  mm_menu_0318112923_0.addMenuItem("Ready&nbsp;Reference&nbsp;Aids","location='http://www.sdc.ucsb.edu/holdings/ready-ref/'");
   mm_menu_0318112923_0.hideOnMouseOut=true;
   mm_menu_0318112923_0.bgColor='#999999';
   mm_menu_0318112923_0.menuBorder=2;
   mm_menu_0318112923_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0318112923_0.menuBorderBgColor='#000000';
  window.mm_menu_0320094412_0 = new Menu("root",262,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0320094412_0.addMenuItem("On-Line&nbsp;Catalog:&nbsp;Pegasus","location='http://pegasus.library.ucsb.edu/'");
  mm_menu_0320094412_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0320094412_0.addMenuItem("USGS&nbsp;Topographic&nbsp;Maps&nbsp;of&nbsp;California","location='http://www.sdc.ucsb.edu/holdings/caltopo.html'");
   mm_menu_0320094412_0.hideOnMouseOut=true;
   mm_menu_0320094412_0.bgColor='#999999';
   mm_menu_0320094412_0.menuBorder=2;
   mm_menu_0320094412_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320094412_0.menuBorderBgColor='#000000';
  window.mm_menu_0320094832_0 = new Menu("root",204,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0320094832_0.addMenuItem("ADL&nbsp;Digital&nbsp;Data&nbsp;Examples","location='http://www.sdc.ucsb.edu/holdings/collectionpage/DataExamples20000516.htm'");
  mm_menu_0320094832_0.addMenuItem("Imagery&nbsp;Examples&nbsp;From&nbsp;MIL","location='http://www.sdc.ucsb.edu/imagery_examples/imagery_examples.html'");
  mm_menu_0320094832_0.addMenuItem("Contact&nbsp;MIL&nbsp;Reference&nbsp;Staff","location='mailto: milrefdesk@library.ucsb.edu'");
   mm_menu_0320094832_0.hideOnMouseOut=true;
   mm_menu_0320094832_0.bgColor='#999999';
   mm_menu_0320094832_0.menuBorder=2;
   mm_menu_0320094832_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320094832_0.menuBorderBgColor='#000000';
window.mm_menu_0320095140_0 = new Menu("root",282,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0320095140_0.addMenuItem("Globetrotter:&nbsp;ADL&nbsp;Webclient","location='http://clients.alexandria.ucsb.edu/globetrotter/'");
  mm_menu_0320095140_0.addMenuItem("ADL&nbsp;Gazetteer&nbsp;(Using&nbsp;ESRI&nbsp;Data&nbsp;Source)","location='http://webclient.alexandria.ucsb.edu/client/gaz/esri/index.jsp'");
  mm_menu_0320095140_0.addMenuItem("ADL&nbsp;Gazetteer&nbsp;(Using&nbsp;ADL&nbsp;Data&nbsp;Source)","location='http://webclient.alexandria.ucsb.edu/client/gaz/adl/index.jsp'");
   mm_menu_0320095140_0.hideOnMouseOut=true;
   mm_menu_0320095140_0.bgColor='#999999';
   mm_menu_0320095140_0.menuBorder=2;
   mm_menu_0320095140_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320095140_0.menuBorderBgColor='#000000';
window.mm_menu_0325123755_0_1 = new Menu("Aerial&nbsp;Photographs",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_1.addMenuItem("&nbsp;&nbsp;Center&nbsp;for&nbsp;Earth&nbsp;and&nbsp;Planetary&nbsp;Studies","location='http://www.nasm.si.edu/research/ceps/rpif/rpifsources.cfm'");
     mm_menu_0325123755_0_1.hideOnMouseOut=true;
     mm_menu_0325123755_0_1.bgColor='#FFFFFF';
     mm_menu_0325123755_0_1.menuBorder=1;
     mm_menu_0325123755_0_1.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_1.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_2 = new Menu("Artificial&nbsp;Satelllites",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_2.addMenuItem("Heavens&nbsp;Above!","location='http://www.heavens-above.com/'");
    mm_menu_0325123755_0_2.addMenuItem("Missions&nbsp;and&nbsp;Spacecraft&nbsp;Library","location='http://leonardo.jpl.nasa.gov/msl/home.html'");
    mm_menu_0325123755_0_2.addMenuItem("Official&nbsp;NASA&nbsp;Swift&nbsp;Homepage","location='http://heasarc.gsfc.nasa.gov/docs/swift/swiftsc.html'");
    mm_menu_0325123755_0_2.addMenuItem("SCScI/HST&nbsp;Public&nbsp;Information","location='http://oposite.stsci.edu/'");
     mm_menu_0325123755_0_2.hideOnMouseOut=true;
     mm_menu_0325123755_0_2.bgColor='#FFFFFF';
     mm_menu_0325123755_0_2.menuBorder=1;
     mm_menu_0325123755_0_2.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_2.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_3 = new Menu("Asteroids",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_3.addMenuItem("Asteroid&nbsp;Satellites","location='http://www.boulder.swri.edu/merline/press/'");
     mm_menu_0325123755_0_3.hideOnMouseOut=true;
     mm_menu_0325123755_0_3.bgColor='#FFFFFF';
     mm_menu_0325123755_0_3.menuBorder=1;
     mm_menu_0325123755_0_3.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_3.menuBorderBgColor='#000000';
      window.mm_menu_0325123755_0_4_1 = new Menu("University&nbsp;of&nbsp;Massachusetts",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
      mm_menu_0325123755_0_4_1.addMenuItem("2MASS&nbsp;Second&nbsp;Incremental&nbsp;Data&nbsp;","location='http://www.ipac.caltech.edu/2mass/gallery/second/'");
      mm_menu_0325123755_0_4_1.addMenuItem("2MASS&nbsp;2&nbsp;Micron&nbsp;All&nbsp;Sky&nbsp;Survey","location='http://pegasus.astro.umass.edu/GradProg/2mass.html'");
      mm_menu_0325123755_0_4_1.addMenuItem("The&nbsp;Two&nbsp;Micron&nbsp;All&nbsp;Sky&nbsp;Survey&nbsp;at&nbsp;IPAC","location='http://www.ipac.caltech.edu/2mass/'");
       mm_menu_0325123755_0_4_1.hideOnMouseOut=true;
       mm_menu_0325123755_0_4_1.bgColor='#FFFFFF';
       mm_menu_0325123755_0_4_1.menuBorder=1;
       mm_menu_0325123755_0_4_1.menuLiteBgColor='#FFFFFF';
       mm_menu_0325123755_0_4_1.menuBorderBgColor='#000000';
      window.mm_menu_0325123755_0_4_2 = new Menu("European&nbsp;Southern&nbsp;Observatory",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
      mm_menu_0325123755_0_4_2.addMenuItem("ESO&nbsp;and&nbsp;Space&nbsp;Telescope","location='http://archive.eso.org/'");
      mm_menu_0325123755_0_4_2.addMenuItem("European&nbsp;Southern&nbsp;Observatory&nbsp;Info","location='http://www.eso.org/outreach/info-events/'");
       mm_menu_0325123755_0_4_2.hideOnMouseOut=true;
       mm_menu_0325123755_0_4_2.bgColor='#FFFFFF';
       mm_menu_0325123755_0_4_2.menuBorder=1;
       mm_menu_0325123755_0_4_2.menuLiteBgColor='#FFFFFF';
       mm_menu_0325123755_0_4_2.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_4 = new Menu("Astronomical-Observations",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_4.addMenuItem(mm_menu_0325123755_0_4_1);
    mm_menu_0325123755_0_4.addMenuItem(mm_menu_0325123755_0_4_2);
    mm_menu_0325123755_0_4.addMenuItem("Infrared&nbsp;Space&nbsp;Observatory&nbsp;U.S.&nbsp;Science&nbsp;Support&nbsp;Center","location='http://www.ipac.caltech.edu/iso/'");
    mm_menu_0325123755_0_4.addMenuItem("Mapping&nbsp;the&nbsp;Heavens:&nbsp;The&nbsp;Next&nbsp;Generation&nbsp;of&nbsp;Celestial&nbsp;Surveys","location='http://spider.ipac.caltech.edu/staff/jarrett/talks/pomona/pres.html'");
    mm_menu_0325123755_0_4.addMenuItem("The&nbsp;Radio&nbsp;Sky&nbsp;at&nbsp;485&nbsp;MHz","location='http://www.parkes.atnf.csiro.au/research/surveys/aitoff.html'");
    mm_menu_0325123755_0_4.addMenuItem("The&nbsp;Spacewatch&nbsp;Project","location='http://spacewatch.lpl.arizona.edu/index.html'");
     mm_menu_0325123755_0_4.hideOnMouseOut=true;
     mm_menu_0325123755_0_4.childMenuIcon="arrows.gif";
     mm_menu_0325123755_0_4.bgColor='#FFFFFF';
     mm_menu_0325123755_0_4.menuBorder=1;
     mm_menu_0325123755_0_4.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_4.menuBorderBgColor='#000000';
  window.mm_menu_0325123755_0 = new Menu("root",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_1);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_2);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_3);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_4);
   mm_menu_0325123755_0.hideOnMouseOut=true;
   mm_menu_0325123755_0.childMenuIcon="arrows.gif";
   mm_menu_0325123755_0.bgColor='#FFFFFF';
   mm_menu_0325123755_0.menuBorder=1;
   mm_menu_0325123755_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0325123755_0.menuBorderBgColor='#000000';

mm_menu_0325123755_0.writeMenus();
} // mmLoadMenus()



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script language="JavaScript" src="../js/mm_menu.js"></script>
</head>

<body>
<script language="JavaScript1.2">mmLoadMenus();</script>
<table width="100%" height="143"  border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td width="73%" height="141" align="left" valign="top" bgcolor="#000000"><div align="center" class="style46">
      <div align="center"><img src="../images/MIL/MIL_BannerII.jpg" width="672" height="133"> </div>
    </div></td>
    <td width="27%" valign="middle" bgcolor="#000000"><div align="center">
      <p class="style63"><span class="style101"><br>
        Business Hours<br>
    Mon. - Fri. <br>
&nbsp;9:00 AM<br>
to
      </span><span class="style101"><br>
          5:00 PM<span class="style100"><span class="style15"><br>
          <br>
          </span></span><span class="style15"><br>
          </span></span></p>
    </div></td>
  </tr>
</table>
<table width="100%"  border="3" cellpadding="2" cellspacing="1" bordercolor="#333333" bgcolor="#CCCCCC">
  <tr class="style33">
    <td width="20%"><div align="center" class="style76 style78"></div>            <div align="center" class="style80"><a href="http://collections.alexandria.ucsb.edu/ap/indexes/" name="link3" id="link5" onMouseOver="MM_showMenu(window.mm_menu_0318112923_0,-8,18,null,'link3')" onMouseOut="MM_startTimeout();">Air Photos</a> </div></td>
    <td width="20%"><div align="center" class="style80"><a href="http://pegasus.library.ucsb.edu/" name="link9" id="link6" onMouseOver="MM_showMenu(window.mm_menu_0320094412_0,-8,18,null,'link9')" onMouseOut="MM_startTimeout();">Maps &amp; Atlases</a> </div></td>
    <td width="20%"><div align="center" class="style80"><a href="http://clients.alexandria.ucsb.edu/globetrotter/" name="link4" id="link10" onMouseOver="MM_showMenu(window.mm_menu_0320095140_0,-8,18,null,'link4')" onMouseOut="MM_startTimeout();">Search Tools</a> </div></td>
    <td width="20%"><div align="center" class="style82"><span class="style80"><a href="http://www.ngda.org/project_archive/milex.php" name="link11" id="link7">External Resources</a></span></div></td>
    <td width="20%"><div align="center" class="style80"><span class="style82"><a href="mailto:%20milrefdesk@library.ucsb.edu" name="link2" id="link8" onMouseOver="MM_showMenu(window.mm_menu_0320094832_0,-35,18,null,'link2')" onMouseOut="MM_startTimeout();">Information &amp; Help</a></span> </div></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="70%" align="left" valign="top"><p class="style97"><br>
      Business hours are Monday through Friday 9am to 5pm. Most Map &amp; Imagery Laboratory (MIL) materials do not circulate. Access to the closed-stack collection is provided via MIL staff and various finding aids. Fees for access to the imagery and digital data collection are charged to those persons not performing University of California (UC) related research.<br>
      <br>
      Some items are listed at this time in <strong><a href="http://pegasus.library.ucsb.edu/F">Pegasus </a></strong>, the Library's on-line catalog. Air photos are being cataloged in the <strong><a href="http://webclient.alexandria.ucsb.edu/">Alexandria Digital Library </a></strong>.</p>      
      <table width="100%"  border="0" cellspacing="3" cellpadding="5">
        <tr>
          <td width="38%" valign="top" class="style11"><span class="style97"><span class="style59"><span class="style95"><span class="style98"><span class="style9 style11 style12"><span class="style9 style3  style34"><span class="style99"><a href="http://www.library.ucsb.edu/"><img src="../images/MIL/library.jpg" alt="UCSB Library" width="191" height="65" border="0" align="baseline"></a></span></span></span></span></span></span></span></td>
          <td width="62%" valign="top" class="style11"><p class="style97">Reproduction policies for uncopyrighted materials vary according to format, size and data source. Consult MIL staff for details. </p>
            <p class="style97">MIL is a U.S. Geological Survey Earth Science Information Center and a NASA Space Shuttle imagery viewing center. </p>            <span class="style97"><br>
                </span></td>
        </tr>
      </table>      
    </td>
    <td width="30%" height="358" align="left" valign="top"><span class="style51"><span class="style97">
      </span></span>
      <table width="250"  border="3" align="right" cellpadding="1" cellspacing="3">
        <tr>
          <td><div align="left"><span class="style51"><span class="style97">      <input type="submit" name="Submit" value="Search">
                  <input type="text" name="textfield">
          </span></span> </div></td>
        </tr>
        <tr>
          <td><div align="center"><img src="../MIL/Boxes/PersonnelBoxes  copy.jpg" width="191" height="190"></div></td>
        </tr>
        <tr>
          <td><p align="center" class="style51"><span class="style19">To make arrangements for specialized services, instruction, tours or workshops please contact the Map and Imagery Lab:</span></p>
          <p align="center" class="style51"><span class="style19"><span class="style9 style12 style11"><span class="style9 style3  style34"><span class="style66">Phone: (805) 893-2779<br>
Fax: (805) 893-8799</span></span></span></span> </p></td>
        </tr>
    </table>    </td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td><table width="100%" height="16"  border="0" cellpadding="0" cellspacing="0" bordercolor="#000000" bgcolor="#CCCCCC">
      <tr>
        <td width="21%"><div align="center"><span class="style3"><span class="style95"><a href="http://www.ngda.org/project_archive/mil.php">MIL Home</a></span></span></div></td>
        <td width="21%"><div align="center"><span class="style3"><span class="style95"><a href="http://www.ngda.org/MIL%20Services.php">MIL Services</a></span></span></div></td>
        <td width="22%"><div align="center"><span class="style3"><span class="style95"><a href="http://www.sdc.ucsb.edu/tour">MIL Lab Tour</a></span></span></div></td>
        <td width="21%"><div align="center"><span class="style3"><span class="style95"><a href="mil_contacts.php">Contact MIL</a></span></span></div></td>
        <td width="15%"><div align="center"><span class="style3"><a href="http://www.ucsb.edu/">UCSB</a></span></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="100%" border="3" cellspacing="1" cellpadding="0">
  <tr>
    <td width="71" rowspan="2" align="left" class="style3 style8"><a href="http://clients.alexandria.ucsb.edu"><img src="../images/MIL/adl.jpg" alt="Alexandria Digital Library" width="71" height="73" border="0"></a></td>
    <td width="91%" height="42" align="left" bgcolor="#070264">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="style9 style11">The MIL is a closed-stack collection; most MIL materials do not circulate.<br>
      Fees for access to the imagery and digital data collection are charged to those persons not performing University of California related research.<br>
      Copyright &copy; 2007 University of California Regents; All Rights Reserved. </td>
  </tr>
</table>
<br>
<table width="100%"  border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="37%" valign="top"><span class="style19">Last Updated</span><br>      <span class="style51"><span class="style92"><span class="style94">
      <!-- #BeginDate format:Am1m -->April 2, 2008  15:08<!-- #EndDate -->
      </span></span></span></td>
    <td width="63%"><div align="right"><span class="style63"><span class="style101"><span class="style102">Map and Imagery Lab,<br>
  Davidson Library<br>
  University of California, <br>
  Santa Barbara, CA<br>
  93106-9010</span></span></span></div></td>
  </tr>
</table>
<br>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
