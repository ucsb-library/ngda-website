<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>UCSB &amp; Stanford University -  NGDA - National Geospatial Digital Archive  - part of NDIIPP</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="stylesheets/ndiipp.css" type=text/css rel=stylesheet>
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<style type="text/css">
<!--
a:link {
	color: #0033FF;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #0099FF;
}
a:hover {
	text-decoration: underline;
	color: #CC0000;
}
a:active {
	text-decoration: none;
}
.style54 {color: #000000}
.style64 {	color: #0000FF;
	font-weight: bold;
	font-size: 24px;
}
body {
	background-color: #000000;
	margin-left: 0px;
	margin-top: 0px;
}
.style65 {color: #0099FF}
-->
</style>
</HEAD>
<BODY>
<table width="782" height="115" align="center" cellpadding="0">
  <tr align="center" bgcolor="#000000">
    <td width="782" height="109"><div align="center">
        <!--END HEADER -->
      </div>
        <div align="center"><img src="images/Templates/topbar_a%20copy.jpg" width="774" height="20"></div>
      <div align="center">&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Templates/ngdasat%20copy2.jpg" width="120" height="58">&nbsp;<span class="style64"><br>
            <span class="style65">National Geospatial Digital Archive</span></span></div></td>
  </tr>
</table>
<table width="101%"  border="0" cellpadding="2" bgcolor="#FFFFFF">
  <tr>
    <td width="20%" height="840" valign="top"><p><span class="style54">
          <map name="Map2">
            <area shape="rect" coords="4,30,158,52" href="index.php">
            <area shape="rect" coords="2,203,134,223" href="participants.php">
            <area shape="rect" coords="4,142,150,162" href="content.php">
            <area shape="rect" coords="6,261,180,281" href="calendar.php">
            <area shape="rect" coords="4,85,150,108" href="research.php">
            <area shape="rect" coords="2,316,155,336" href="meetings.php">
            <area shape="rect" coords="4,491,107,511" href="contact.php">
            <area shape="rect" coords="4,431,134,457" href="junk/links.php">
            <area shape="rect" coords="5,374,143,392" href="http://www.digitalpreservation.gov/">
          </map>
    </span><img src="images/Templates/linksbox%20copy%202.jpg" width="144" height="504" border="0" usemap="#Map4">
        <map name="Map4">
          <area shape="rect" coords="12,372,118,403" href="contact.php">
          <area shape="rect" coords="12,322,133,343" href="http://www.digitalpreservation.gov/index.php?nav=1">
          <area shape="rect" coords="14,266,105,288" href="meetings.php">
          <area shape="rect" coords="13,214,120,238" href="participants.php">
          <area shape="rect" coords="10,151,119,186" href="research.php">
          <area shape="rect" coords="10,85,118,121" href="content.php">
          <area shape="rect" coords="13,31,128,50" href="index.php">
        </map>
          <span class="style54"> </span></p>
    </td>
    <td width="77%" valign="top" bgcolor="#FFFFFF"><p align="center" class="style64">Content Providers </p>
        <p class="style54">&nbsp;</p>
        <p align="left"><strong>In the first phase of the project, we will be working with two well documented test collections of endangered digital geospatial data.&nbsp; UCSB will be collaborating with the <a href="http://gis.ca.gov">California Spatial Information Library </a>, the leading Web portal for governmentally produced geospatial information about California.&nbsp; Stanford will be collaborating with the <a href="http://www.davidrumsey.com/">David Rumsey Collection </a>.&nbsp; The Rumsey Collection has over 11,000 images online with a focus on 18th and 19th century North and South American maps and other cartographic materials. <br>
  Over the course of the grant other collections will be added.&nbsp; We hope that those having such collections will contact us. </strong></p>
        <p><strong>We will also be looking for several well documented test collections of endangered digital geospatial data. We hope that those having such collections will contact us. These entries will be vetted by a group of specialist who will evaluate and prioritize the content for addition to the Geospatial Digital Repository. It is important that the intellectual property be clearly defined, that each data object have good documentation and that the digital format be well understood. </strong></p>
        <p><strong>Soon we will have a set of specifications that will better allow providers to determine if their content is in proper condition for adding to the Repository. As our understanding of how best to accomplish the goals set forth in our proposal increases, we will be looking for more and more complex datasets for ingesting content. </strong></p></td>
    <td width="3%" align="left" valign="top" bgcolor="#FFFFFF"><p><img src="images/Templates/partban.jpg" width="102" height="800"></p>
    </td>
  </tr>
</table>
<map name="Map3Map">
  <area shape="rect" coords="4,13,156,33" href="index.php">
  <area shape="rect" coords="6,47,154,69" href="participants.php">
  <area shape="rect" coords="9,78,155,100" href="content.php">
  <area shape="rect" coords="8,115,156,140" href="meetings.php">
  <area shape="rect" coords="5,151,163,176" href="research.php">
  <area shape="rect" coords="9,189,161,212" href="calendar.php">
  <area shape="rect" coords="11,263,147,289" href="junk/links.php">
  <area shape="rect" coords="8,299,152,323" href="contact.php">
  <area shape="rect" coords="11,227,149,250" href="http://www.digitalpreservation.gov/index.php?nav=1">
</map>
<map name="Map">
  <area shape="rect" coords="9,8,172,25" href="http://www.ngda.org/">
  <area shape="rect" coords="5,35,146,56" href="http://www.ngda.org/participants.php">
  <area shape="rect" coords="77,62,78,63" href="#">
  <area shape="rect" coords="6,94,167,113" href="http://www.ngda.org/calendar.php">
  <area shape="rect" coords="4,62,158,86" href="http://www.ngda.org/content.php">
  <area shape="rect" coords="7,122,175,144" href="research.php">
  <area shape="rect" coords="5,150,204,174" href="meetings.php">
  <area shape="rect" coords="6,182,174,207" href="http://www.ngda.org/junk/links.php">
  <area shape="rect" coords="9,215,182,237" href="http://www.ngda.org/ndiipp.php">
  <area shape="rect" coords="9,245,185,263" href="http://www.ngda.org/contact.php">
</map>
</BODY></HTML>
