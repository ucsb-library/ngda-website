<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>UCSB &amp; Stanford University - NGDA - National Geospatial Digital Archive</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="images/ndiipp.css" type="text/css" rel="stylesheet">
	<meta content="MSHTML 6.00.2900.2180" name="GENERATOR">
<style type="text/css">
<!--
a:link {
	color: #FFFFFF;
	text-decoration: none;
	font-weight: bold;
}
a:visited {
	color: #CCCCCC;
	text-decoration: none;
}
a:hover {
	color: #FF0000;
	text-decoration: none;
}
.style68 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style65 {color: #000000}
.style111 {font-size: 14px}
.style122 {font-size: 18px; color: #000000; font-weight: bold;}
body {
	margin-left: 5px;
	margin-top: 5px;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000066;
}
.style179 {color: #000000; font-size: 14px; }
.style184 {color: #000066; font-size: 16px;}
.style185 {font-size: 14px; font-weight: bold; color: #000000; }
.style191 {font-family: "Times New Roman", Times, serif; font-size: 16px;}
.style192 {color: #000066; font-size: 16px; font-weight: bold; }
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #FFFFFF;
	font-weight: bold;
}
a:active {
	text-decoration: none;
}
.style225 {color: #000000; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.style23 {font-size: 10px}
.style16 {color: #666666}
.style67 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000099;}
.style274 {font-family: "Times New Roman", Times, serif; font-size: 14px; }
.style275 {font-family: Arial, Helvetica, sans-serif}
.style277 {font-size: 12px; color: #000000; }
.style72 {font-size: 14px; font-family: Arial, Helvetica, sans-serif; }
.style297 {font-style: italic}
.style298 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-style: italic; }
.style299 {
	font-size: 24px;
	font-weight: bold;
}
.style301 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; }
-->
</style> <script language="JavaScript" type="application/javascript">
<!--
function mmLoadMenus() {
  if (window.mm_menu_0713134339_0) return;
            window.mm_menu_0713134339_0 = new Menu("root",171,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713134339_0.addMenuItem("Journals&nbsp;and&nbsp;Papers","location='papers.php'");
  mm_menu_0713134339_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0713134339_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0713134339_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0713134339_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='research.php#RR'");
   mm_menu_0713134339_0.hideOnMouseOut=true;
   mm_menu_0713134339_0.bgColor='#555555';
   mm_menu_0713134339_0.menuBorder=1;
   mm_menu_0713134339_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713134339_0.menuBorderBgColor='#777777';
window.mm_menu_0713140001_0 = new Menu("root",141,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140001_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0713140001_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0713140001_0.addMenuItem("2005&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0713140001_0.hideOnMouseOut=true;
   mm_menu_0713140001_0.bgColor='#555555';
   mm_menu_0713140001_0.menuBorder=1;
   mm_menu_0713140001_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140001_0.menuBorderBgColor='#777777';
  window.mm_menu_0713140152_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140152_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='meetings.php#DPW'");
  mm_menu_0713140152_0.addMenuItem("Collections&nbsp;Workshop","location='meetings.php#CW'");
  mm_menu_0713140152_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='meetings.php#EAB'");
   mm_menu_0713140152_0.hideOnMouseOut=true;
   mm_menu_0713140152_0.bgColor='#555555';
   mm_menu_0713140152_0.menuBorder=1;
   mm_menu_0713140152_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140152_0.menuBorderBgColor='#777777';

mm_menu_0713140152_0.writeMenus();
} // mmLoadMenus()

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script> <script language="JavaScript" src="/js/mm_menu.js" type="application/javascript"></script> 
</head>
<body onLoad="MM_preloadImages('images/templates/nb_home.jpg','images/templates/nb_home.jpg','images/templates/nb_home.jpg','images/templates/nb_research.jpg','images/templates/nb_research.jpg','images/templates/nb_research.jpg')">

	<div align="left">
		<p>&nbsp;
			 
		</p>
	</div>
	<map name="Map">
		<area shape="rect" coords="317,155,382,170" href="research.php" alt="Research" onMouseOver="MM_showMenu(window.mm_menu_0713134339_0,317,190,null,'image1');MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_startTimeout();;MM_swapImgRestore()">
		<area shape="rect" coords="403,156,476,172" href="project_archive/NGDA_participants.php" alt="Participants" onMouseOver="MM_showMenu(window.mm_menu_0713140001_0,403,190,null,'image1');MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_startTimeout();MM_swapImgRestore()">
		<area shape="rect" coords="501,156,567,172" href="meetings.php" alt="Meetings" onMouseOver="MM_showMenu(window.mm_menu_0713140152_0,501,190,null,'image1');MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_startTimeout();MM_swapImgRestore()">
		<area shape="rect" coords="235,156,283,173" href="index.php" alt="Home" onMouseOver="MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_swapImgRestore()">
		<area shape="rect" coords="587,158,662,172" href="contact.php" alt="Contact Us" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
		<area shape="rect" coords="689,157,743,169" href="http://www.digitalpreservation.gov/" alt="NDIIPP" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
	</map>
</div>

	<span class="style274"><a href="http://www.universityofcalifornia.edu/news/globalwarming.pdf"><img src="images/index/global%20warming.jpg" alt="UC Global Warming Initiative" width="400" height="92" border="0" align="middle"></a></span>
</div>

	<span class="style67"><span class="style16"><span class="style65"> </span></span></span>
</div>

</div>

	<span class="style67"><span class="style16"><span class="style65"><img src="images/Templates/PhaseIIBottom.jpg" width="740" height="90"></span></span></span>
</div>

	<div align="left">
		<span class="style164 style322 style23 style295"><font color="#CCCCCC" size="1"><strong>Copyright &copy; 2005-2007 
					<br>
					University of California,
					<br>
					Santa Barbara, CA 93106 
					<br>
					(805) 893-8000</strong></font></span>
	</div>
</div>

	<div align="right">
		<span class="style164 style322 style23 style295"><font color="#CCCCCC" size="1"><strong>Last Modified
					<br>
<!-- #BeginDate format:Am1a -->
					November 5, 2009 12:21 PM
<!-- #EndDate -->
				</strong></font></span>
	</div>
</div>

	<strong><span class="style191"><span class="bodylink"><span class="style179"><span class="style111"><span class="style184"><span class="bodylink style68"><span class="style301"><a href="http://www.digitalpreservation.gov/">The National Digital Information Infrastructure and Preservation Program</a></span></span></span></span></span></span></span></strong>
</div>

	<a href="http://www.stanford.edu/">Stanford University</a>
</div>

	<a href="http://www-sul.stanford.edu/">Stanford University
		<br>
		Libraries</a>
</div>

	<span class="style111"><a href="http://library.stanford.edu/depts/branner/index.html">Branner Earth Sciences</a></span>
</div>

	<a href="http://library.stanford.edu/depts/dlss/collections/sdr.htm">Stanford Digital Repository </a>
</div>

	<a href="http://www.ucsb.edu/">UCSB</a>
</div>

	<a href="http://www.alexandria.ucsb.edu/">Alexandria Digital 
		<br>
		Library</a>
</div>

	<a href="http://www.sdc.ucsb.edu/">Map and Imagery Lab</a>
</div>

	<h1 class="style299">Contact NGDA</h1> <span class="style72">Please contact us if you are interested in knowing more about the project of if you have 
		<br>
		any questions or would further information about the National Geospatial Digital Archive project</span>
</div>

	<div align="center" class="addr">
		<span class="style277"><span class="style191"> <a href="mailto:bjohnson@library.ucsb.edu%20">bjohnson@library.ucsb.edu</a> </span></span>
	</div>
</div>

	<div align="left">
		<p>
			<span class="style68"><em><strong> Principal Investigator, UCSB </strong></em></span><strong><span class="style191"><span class="style111">
						<br>
					</span></span></strong><span class="style274"><span class="style275"> Brenda Johnson </span></span><strong><span class="style191"><span class="style111">
						<br>
					</span></span></strong><span class="style277"> University of California, Santa Barbara
				<br>
				University Librarian 
				<br>
				<span class="style191"><span class="bodylink"><span class="style225"> (805) 893-3256 </span></span></span></span>
		</p>
	</div>
</div>

	<div class="addr" align="center">
		<span class="style277"><strong><span class="style191"><span class="bodylink"><span class="style179"><span class="style111"><a href="mailto:sweetkind@stanford.edu"> sweetkind@stanford.edu</a></span></span></span></span></strong> </span>
	</div>
</div>

	<span class="style68"><span class="style297">Program Director, Stanford </span> </span> 
	<br>
	<span class="style111"> Julie Sweetkind-Singer 
		<br>
	</span> <span class="style277"> Stanford University, 
		<br>
		Head Librarian, Library &amp; Map Collections</span>
</div>

	<img src="images/Templates/New%20Banner.jpg" name="image1" width="760" height="203" border="0" usemap="#Map" id="image1">
</div>
</body>
</html>
