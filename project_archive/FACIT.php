<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD>
<TITLE>UCSB&amp;Stanford University - NGDA - National Geospatial Digital Archive - part of NDIIPP</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<LINK 
href="stylesheets/ndiipp.css" type=text/css rel=stylesheet>
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<style type="text/css">
<!--
a:link {
	color: #FFFFFF;
	text-decoration: none;
	font-weight: bold;
}
a:visited {
	text-decoration: none;
	color: #FFFFFF;
}
a:hover {
	text-decoration: none;
	color: #FF0000;
}
a:active {
	text-decoration: none;
}
body {
	background-color: #FFFFFF;
	margin-left: 5px;
	margin-top: 5px;
}
a {
	font-size: 11px;
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style16 {color: #666666}
.style23 {font-size: 10px}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
.style67 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000099;}
.style68 {color: #000000}
.style296 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000066; }
.style81 {font-size: 14px;
	font-family: "Times New Roman", Times, serif;
}
.style295 {font-family: Arial, Helvetica, sans-serif}
.style176 {font-size: 11px}
.style177 {font-weight: bold; font-style: italic; color: #CCCCCC;}
.style311 {font-weight: bold; font-size: 12px; color: #CCCCCC;}
.style313 {color: #FFFFFF}
.style181 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #180289; font-size: 24px; }
.style314 {	color: #FF6600;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style183 {color: #FFFFFF; font-size: 12px; }
.style184 {
	font-size: 16px;
	font-weight: bold;
	color: #180289;
}
.style187 {color: #180289}
.style190 {font-size: 14px; font-family: Arial, Helvetica, sans-serif;}
.style192 {font-weight: bold; font-size: 24px; font-family: Arial, Helvetica, sans-serif;}
.style193 {color: #000066}
-->
</style>
<script language="JavaScript">
<!--

function mmLoadMenus() {
  if (window.mm_menu_0911104444_0) return;
        window.mm_menu_0911104444_0 = new Menu("root",129,12,"Arial, Helvetica, sans-serif",12,"#000000","#999999","#CCCCCC","#FFFFFF","left","middle",0,0,500,-5,7,true,false,true,0,true,true);
  mm_menu_0911104444_0.addMenuItem("Progress&nbsp;Reports","location='research.php'");
  mm_menu_0911104444_0.addMenuItem("Meetings","location='meetings.php'");
  mm_menu_0911104444_0.addMenuItem("Project&nbsp;Reviews","location='reviews.php'");
   mm_menu_0911104444_0.hideOnMouseOut=true;
   mm_menu_0911104444_0.bgColor='#CCCCCC';
   mm_menu_0911104444_0.menuBorder=1;
   mm_menu_0911104444_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0911104444_0.menuBorderBgColor='#CCCCCC';

          window.mm_menu_0524113534_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0524113534_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='meetings.php#dpw'");
  mm_menu_0524113534_0.addMenuItem("Collections&nbsp;Workshop","location='meetings.php#cw'");
  mm_menu_0524113534_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='meetings.php#EAB'");
   mm_menu_0524113534_0.fontWeight="bold";
   mm_menu_0524113534_0.hideOnMouseOut=true;
   mm_menu_0524113534_0.bgColor='#555555';
   mm_menu_0524113534_0.menuBorder=1;
   mm_menu_0524113534_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0524113534_0.menuBorderBgColor='#777777';
    window.mm_menu_0525110132_0 = new Menu("root",141,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0525110132_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0525110132_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0525110132_0.addMenuItem("2005&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0525110132_0.fontWeight="bold";
   mm_menu_0525110132_0.hideOnMouseOut=true;
   mm_menu_0525110132_0.bgColor='#555555';
   mm_menu_0525110132_0.menuBorder=1;
   mm_menu_0525110132_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0525110132_0.menuBorderBgColor='#777777';
window.mm_menu_0524141709_0 = new Menu("root",171,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0524141709_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0524141709_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0524141709_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0524141709_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='research.php#RR'");
   mm_menu_0524141709_0.fontWeight="bold";
   mm_menu_0524141709_0.hideOnMouseOut=true;
   mm_menu_0524141709_0.bgColor='#555555';
   mm_menu_0524141709_0.menuBorder=1;
   mm_menu_0524141709_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0524141709_0.menuBorderBgColor='#777777';

  window.mm_menu_0713134339_0 = new Menu("root",171,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713134339_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0713134339_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0713134339_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0713134339_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='research.php#RR'");
   mm_menu_0713134339_0.hideOnMouseOut=true;
   mm_menu_0713134339_0.bgColor='#555555';
   mm_menu_0713134339_0.menuBorder=1;
   mm_menu_0713134339_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713134339_0.menuBorderBgColor='#777777';

  window.mm_menu_0713140001_0 = new Menu("root",141,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140001_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0713140001_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0713140001_0.addMenuItem("2005&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0713140001_0.hideOnMouseOut=true;
   mm_menu_0713140001_0.bgColor='#555555';
   mm_menu_0713140001_0.menuBorder=1;
   mm_menu_0713140001_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140001_0.menuBorderBgColor='#777777';

  window.mm_menu_0713140152_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140152_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='meetings.php#DPW'");
  mm_menu_0713140152_0.addMenuItem("Collections&nbsp;Workshop","location='meetings.php#CW'");
  mm_menu_0713140152_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='meetings.php#EAB'");
   mm_menu_0713140152_0.hideOnMouseOut=true;
   mm_menu_0713140152_0.bgColor='#555555';
   mm_menu_0713140152_0.menuBorder=1;
   mm_menu_0713140152_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140152_0.menuBorderBgColor='#777777';

                        window.mm_menu_0731104816_0 = new Menu("root",181,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000066","#000066","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0731104816_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0731104816_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0731104816_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0731104816_0.addMenuItem("Rights&nbsp;and&nbsp;Responsibilities","location='research.php#RR'");
   mm_menu_0731104816_0.hideOnMouseOut=true;
   mm_menu_0731104816_0.bgColor='#555555';
   mm_menu_0731104816_0.menuBorder=1;
   mm_menu_0731104816_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0731104816_0.menuBorderBgColor='#777777';
window.mm_menu_0731105320_0 = new Menu("root",141,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000066","#000066","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0731105320_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0731105320_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0731105320_0.addMenuItem("2005&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0731105320_0.hideOnMouseOut=true;
   mm_menu_0731105320_0.bgColor='#555555';
   mm_menu_0731105320_0.menuBorder=1;
   mm_menu_0731105320_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0731105320_0.menuBorderBgColor='#777777';
window.mm_menu_0731105633_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000066","#000066","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0731105633_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='meetings.php#DPW'");
  mm_menu_0731105633_0.addMenuItem("Collections&nbsp;Workshop","location='meetings.php#CW'");
  mm_menu_0731105633_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='meetings.php#EAB'");
   mm_menu_0731105633_0.hideOnMouseOut=true;
   mm_menu_0731105633_0.bgColor='#555555';
   mm_menu_0731105633_0.menuBorder=1;
   mm_menu_0731105633_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0731105633_0.menuBorderBgColor='#777777';

mm_menu_0731105633_0.writeMenus();
} // mmLoadMenus()
//-->
</script>
<script language="JavaScript" src="js/mm_menu.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</HEAD>
<BODY onLoad="MM_preloadImages('images/templates/nb_home.jpg','images/templates/nb_home.jpg','images/templates/nb_research.jpg','images/templates/nb_home.jpg','images/templates/nb_research.jpg','images/templates/nb_research.jpg')">
<table width="769" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="720"><div align="center"><img src="images/Templates/New%20Banner.jpg" name="image1" width="760" height="203" border="0" usemap="#Map2" id="image1"> </div></td>
  </tr>
</table>
<map name="MapMap">
  <area shape="rect" coords="317,155,382,170" href="research.php" alt="Research" onMouseOver="MM_showMenu(window.mm_menu_0713134339_0,317,170,null,'image1')" onMouseOut="MM_startTimeout();">
  <area shape="rect" coords="403,156,476,172" href="project_archive/NGDA_participants.php" alt="Participants" onMouseOver="MM_showMenu(window.mm_menu_0713140001_0,403,172,null,'image1')" onMouseOut="MM_startTimeout();">
  <area shape="rect" coords="501,156,567,172" href="meetings.php" alt="Meetings" onMouseOver="MM_showMenu(window.mm_menu_0713140152_0,501,172,null,'image1')" onMouseOut="MM_startTimeout();">
  <area shape="rect" coords="235,156,283,173" href="index.php" alt="Home">
  <area shape="rect" coords="587,158,662,172" href="contact.php" alt="Contact Us">
  <area shape="rect" coords="689,157,743,169" href="http://www.digitalpreservation.gov/" alt="NDIIPP">
</map>
<table>
  <tr align="right" bordercolor="#000033">
    <td width="20%" rowspan="3" valign="top" background="images/templates/sidebar.jpg"><div align="center" class="style311">
        <div align="left" class="style313"></div>
      </div>
      <div align="left">
          <table width="100%"  border="3" align="center" cellpadding="3" cellspacing="1">
            <tr>
              <td><div align="left" class="style314"><span class="style183">NGDA Developments </span></div></td>
            </tr>
            <tr>
              <td height="29">&nbsp;</td>
            </tr>
            <tr>
              <td><span class="style176"><a href="http://clients.alexandria.ucsb.edu/globetrotter/">NGDA<br>
Interface (Version 1.0)</a></span></td>
            </tr>
            <tr>
              <td><span class="style176"><a href="http://clients.alexandria.ucsb.edu/ngda/help.html">NGDA Interface Information</a></span></td>
            </tr>
            <tr>
              <td><span class="style176"><a href="http://ngda.library.ucsb.edu/format">NGDA<br>
Format Registry</a> </span></td>
            </tr>
          </table>
        <span class="style176"><span class="style177"><u><br>
        </u></span><br>
<a href="http://clients.alexandria.ucsb.edu/ngda/help.html"><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
<br>
</a> <br>
<br>
<br>
<a href="http://ngda.library.ucsb.edu/format">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>
<a href="http://ngda.library.ucsb.edu/format">&nbsp;</a><br>
</span></div></td>
    <td colspan="2" valign="top"><div align="left"></div>
        <div align="left">
          <div align="left" class="style181"> </div>
          <div align="left" class="style187">
            <div align="left"></div>
          </div>
          </div></td>
  </tr>
</table>
<br>
<div align="left"><br>
</div>
<div align="center"></div>
<p>&nbsp;</p>
<br>
<p>&nbsp;</p>
<p><script language="JavaScript1.2">mmLoadMenus();</script>
</p>
<map name="Map">
  <area shape="rect" coords="82,12,128,35" href="index.php">
  <area shape="rect" coords="210,13,274,33" href="research.php">
  <area shape="rect" coords="335,12,419,34" href="project_archive/NGDA_participants.php">
  <area shape="rect" coords="477,13,536,34" href="meetings.php">
  <area shape="rect" coords="605,8,675,41" href="contact.php">
</map>
<map name="Map2">
  <area shape="rect" coords="236,154,278,175" href="index.php" alt="Home" onMouseOver="MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="316,155,383,175" href="research.php" alt="Research" onMouseOver="MM_showMenu(window.mm_menu_0731104816_0,270,200,null,'image1');MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_startTimeout();;MM_swapImgRestore()">
  <area shape="rect" coords="404,157,482,173" href="project_archive/NGDA_participants.php" alt="Participants" onMouseOver="MM_showMenu(window.mm_menu_0731105320_0,404,200,null,'image1');MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_startTimeout();;MM_swapImgRestore()">
  <area shape="rect" coords="505,157,565,173" href="meetings.php" alt="Meetings" onMouseOver="MM_showMenu(window.mm_menu_0731105633_0,503,200,null,'image1');MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_startTimeout();;MM_swapImgRestore()">
  <area shape="rect" coords="587,157,655,172" href="contact.php" alt="Contact Us" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="684,154,738,174" href="http://www.digitalpreservation.gov/" alt="NDIIPP" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
</map>
</BODY>
</HTML>
