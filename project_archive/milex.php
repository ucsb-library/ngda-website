<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Map&amp;Imagery</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style3 {color: #FFFFFF}
a:link {
	color: #0033FF;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #FF0000;
}
a:hover {
	text-decoration: none;
	color: #FFFF00;
}
a:active {
	text-decoration: none;
}
.style11 {color: #000000}
.style51 {font-size: 16; font-family: "Times New Roman", Times, serif;}
.style63 {font-family: Arial, Helvetica, sans-serif; color: #000000; }
a {
	font-size: 12px;
	color: #FF6600;
}
.style95 {font-size: 18px; font-weight: bold;}
.style97 {
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style101 {font-size: 10px; color: #FFFFFF; }
.style102 {font-size: 10px; color: #000000; }
body {
	margin-left: 0px;
	margin-top: 0px;
}
.style113 {color: #000099}
.style114 {font-family: Vivaldi; color: #FFCC99; font-style: italic; font-size: 18px;}
.style126 {font-family: "Times New Roman", Times, serif; color: #FF9900;}
.style127 {font-family: Arial, Helvetica, sans-serif; font-size: 9px;}
.style128 {font-family: "Times New Roman", Times, serif; font-size: 9px; color: #FF9900;}
.style130 {font-size: 12}
.style131 {
	font-size: 9px;
	font-family: "Courier New", Courier, mono;
}
.style132 {font-family: "Courier New", Courier, mono}
.style133 {
	font-size: 10px;
	font-family: "Courier New", Courier, mono;
}
.style134 {
	font-family: Vivaldi;
	font-size: 36px;
	color: #FFFFFF;
}
.style137 {font-size: 9px}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--

function mmLoadMenus() {
  if (window.mm_menu_0318111657_0) return;
  window.mm_menu_0318111657_0 = new Menu("root",230,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#333333","#333333","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0318111657_0.addMenuItem("Pegasus:&nbsp;On-Line&nbsp;Catalog","location='http://pegasus-16.library.ucsb.edu/F/?func=find-b-0'");
  mm_menu_0318111657_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0318111657_0.addMenuItem("Air&nbsp;Photos:&nbsp;Ready&nbsp;Reference&nbsp;Aids","location='http://www.sdc.ucsb.edu/holdings/ready-ref/'");
   mm_menu_0318111657_0.hideOnMouseOut=true;
   mm_menu_0318111657_0.bgColor='#555555';
   mm_menu_0318111657_0.menuBorder=1;
   mm_menu_0318111657_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0318111657_0.menuBorderBgColor='#777777';

            window.mm_menu_0318112034_0 = new Menu("root",195,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000000","#000000","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0318112034_0.addMenuItem("Pegasus&nbsp;On-Line&nbsp;Catalog","location='http://pegasus-16.library.ucsb.edu/F/?func=find-b-0'");
  mm_menu_0318112034_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0318112034_0.addMenuItem("Air&nbsp;Photos&nbsp;Ready&nbsp;Reference","location='http://www.sdc.ucsb.edu/holdings/ready-ref/'");
   mm_menu_0318112034_0.hideOnMouseOut=true;
   mm_menu_0318112034_0.bgColor='#CCCCCC';
   mm_menu_0318112034_0.menuBorder=1;
   mm_menu_0318112034_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0318112034_0.menuBorderBgColor='#333333';

                          window.mm_menu_0320094412_0 = new Menu("root",262,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","center","middle",3,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0320094412_0.addMenuItem("On-Line&nbsp;Catalog:&nbsp;Pegasus","location='http://pegasus.library.ucsb.edu/'");
  mm_menu_0320094412_0.addMenuItem("Map&nbsp;Inventories","location='http://www.sdc.ucsb.edu/holdings/map_inventories/'");
  mm_menu_0320094412_0.addMenuItem("USGS&nbsp;Topographic&nbsp;Maps&nbsp;of&nbsp;California","location='http://www.sdc.ucsb.edu/holdings/caltopo.html'");
   mm_menu_0320094412_0.hideOnMouseOut=true;
   mm_menu_0320094412_0.bgColor='#999999';
   mm_menu_0320094412_0.menuBorder=2;
   mm_menu_0320094412_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320094412_0.menuBorderBgColor='#000000';
window.mm_menu_0320094832_0 = new Menu("root",204,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","center","middle",3,1,200,-5,7,true,true,false,0,true,true);
  mm_menu_0320094832_0.addMenuItem("ADL&nbsp;Digital&nbsp;Data&nbsp;Examples","location='http://www.sdc.ucsb.edu/holdings/collectionpage/DataExamples20000516.htm'");
  mm_menu_0320094832_0.addMenuItem("Imagery&nbsp;Examples&nbsp;From&nbsp;MIL","location='http://www.sdc.ucsb.edu/imagery_examples/imagery_examples.html'");
  mm_menu_0320094832_0.addMenuItem("Contact&nbsp;MIL&nbsp;Reference&nbsp;Staff","location='mailto: milrefdesk@library.ucsb.edu'");
   mm_menu_0320094832_0.hideOnMouseOut=true;
   mm_menu_0320094832_0.bgColor='#999999';
   mm_menu_0320094832_0.menuBorder=2;
   mm_menu_0320094832_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320094832_0.menuBorderBgColor='#000000';
window.mm_menu_0320095140_0 = new Menu("root",183,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000000","#000000","#FFFFFF","center","middle",3,1,200,-5,7,true,true,false,0,false,true);
  mm_menu_0320095140_0.addMenuItem("Library&nbsp;Catalog","location='http://pegasus-16.library.ucsb.edu/F/?func=find-b-0'");
  mm_menu_0320095140_0.addMenuItem("Globetrotter:&nbsp;ADL&nbsp;Webclient","location='http://clients.alexandria.ucsb.edu/globetrotter/'");
  mm_menu_0320095140_0.addMenuItem("ADL&nbsp;Gazetteer&nbsp;(ESRI)","location='http://webclient.alexandria.ucsb.edu/client/gaz/esri/index.jsp'");
  mm_menu_0320095140_0.addMenuItem("ADL&nbsp;Gazetteer&nbsp;(ADL)","location='http://webclient.alexandria.ucsb.edu/client/gaz/adl/index.jsp'");
   mm_menu_0320095140_0.hideOnMouseOut=true;
   mm_menu_0320095140_0.bgColor='#999999';
   mm_menu_0320095140_0.menuBorder=2;
   mm_menu_0320095140_0.menuLiteBgColor='#CCCCCC';
   mm_menu_0320095140_0.menuBorderBgColor='#000000';
window.mm_menu_0325123755_0_1 = new Menu("Aerial&nbsp;Photographs",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_1.addMenuItem("&nbsp;&nbsp;Center&nbsp;for&nbsp;Earth&nbsp;and&nbsp;Planetary&nbsp;Studies","location='http://www.nasm.si.edu/research/ceps/rpif/rpifsources.cfm'");
     mm_menu_0325123755_0_1.hideOnMouseOut=true;
     mm_menu_0325123755_0_1.bgColor='#FFFFFF';
     mm_menu_0325123755_0_1.menuBorder=1;
     mm_menu_0325123755_0_1.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_1.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_2 = new Menu("Artificial&nbsp;Satelllites",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_2.addMenuItem("Heavens&nbsp;Above!","location='http://www.heavens-above.com/'");
    mm_menu_0325123755_0_2.addMenuItem("Missions&nbsp;and&nbsp;Spacecraft&nbsp;Library","location='http://leonardo.jpl.nasa.gov/msl/home.html'");
    mm_menu_0325123755_0_2.addMenuItem("Official&nbsp;NASA&nbsp;Swift&nbsp;Homepage","location='http://heasarc.gsfc.nasa.gov/docs/swift/swiftsc.html'");
    mm_menu_0325123755_0_2.addMenuItem("SCScI/HST&nbsp;Public&nbsp;Information","location='http://oposite.stsci.edu/'");
     mm_menu_0325123755_0_2.hideOnMouseOut=true;
     mm_menu_0325123755_0_2.bgColor='#FFFFFF';
     mm_menu_0325123755_0_2.menuBorder=1;
     mm_menu_0325123755_0_2.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_2.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_3 = new Menu("Asteroids",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_3.addMenuItem("Asteroid&nbsp;Satellites","location='http://www.boulder.swri.edu/merline/press/'");
     mm_menu_0325123755_0_3.hideOnMouseOut=true;
     mm_menu_0325123755_0_3.bgColor='#FFFFFF';
     mm_menu_0325123755_0_3.menuBorder=1;
     mm_menu_0325123755_0_3.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_3.menuBorderBgColor='#000000';
      window.mm_menu_0325123755_0_4_1 = new Menu("University&nbsp;of&nbsp;Massachusetts",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
      mm_menu_0325123755_0_4_1.addMenuItem("2MASS&nbsp;Second&nbsp;Incremental&nbsp;Data&nbsp;","location='http://www.ipac.caltech.edu/2mass/gallery/second/'");
      mm_menu_0325123755_0_4_1.addMenuItem("2MASS&nbsp;2&nbsp;Micron&nbsp;All&nbsp;Sky&nbsp;Survey","location='http://pegasus.astro.umass.edu/GradProg/2mass.html'");
      mm_menu_0325123755_0_4_1.addMenuItem("The&nbsp;Two&nbsp;Micron&nbsp;All&nbsp;Sky&nbsp;Survey&nbsp;at&nbsp;IPAC","location='http://www.ipac.caltech.edu/2mass/'");
       mm_menu_0325123755_0_4_1.hideOnMouseOut=true;
       mm_menu_0325123755_0_4_1.bgColor='#FFFFFF';
       mm_menu_0325123755_0_4_1.menuBorder=1;
       mm_menu_0325123755_0_4_1.menuLiteBgColor='#FFFFFF';
       mm_menu_0325123755_0_4_1.menuBorderBgColor='#000000';
      window.mm_menu_0325123755_0_4_2 = new Menu("European&nbsp;Southern&nbsp;Observatory",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
      mm_menu_0325123755_0_4_2.addMenuItem("ESO&nbsp;and&nbsp;Space&nbsp;Telescope","location='http://archive.eso.org/'");
      mm_menu_0325123755_0_4_2.addMenuItem("European&nbsp;Southern&nbsp;Observatory&nbsp;Info","location='http://www.eso.org/outreach/info-events/'");
       mm_menu_0325123755_0_4_2.hideOnMouseOut=true;
       mm_menu_0325123755_0_4_2.bgColor='#FFFFFF';
       mm_menu_0325123755_0_4_2.menuBorder=1;
       mm_menu_0325123755_0_4_2.menuLiteBgColor='#FFFFFF';
       mm_menu_0325123755_0_4_2.menuBorderBgColor='#000000';
    window.mm_menu_0325123755_0_4 = new Menu("Astronomical-Observations",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
    mm_menu_0325123755_0_4.addMenuItem(mm_menu_0325123755_0_4_1);
    mm_menu_0325123755_0_4.addMenuItem(mm_menu_0325123755_0_4_2);
    mm_menu_0325123755_0_4.addMenuItem("Infrared&nbsp;Space&nbsp;Observatory&nbsp;U.S.&nbsp;Science&nbsp;Support&nbsp;Center","location='http://www.ipac.caltech.edu/iso/'");
    mm_menu_0325123755_0_4.addMenuItem("Mapping&nbsp;the&nbsp;Heavens:&nbsp;The&nbsp;Next&nbsp;Generation&nbsp;of&nbsp;Celestial&nbsp;Surveys","location='http://spider.ipac.caltech.edu/staff/jarrett/talks/pomona/pres.html'");
    mm_menu_0325123755_0_4.addMenuItem("The&nbsp;Radio&nbsp;Sky&nbsp;at&nbsp;485&nbsp;MHz","location='http://www.parkes.atnf.csiro.au/research/surveys/aitoff.html'");
    mm_menu_0325123755_0_4.addMenuItem("The&nbsp;Spacewatch&nbsp;Project","location='http://spacewatch.lpl.arizona.edu/index.html'");
     mm_menu_0325123755_0_4.hideOnMouseOut=true;
     mm_menu_0325123755_0_4.childMenuIcon="arrows.gif";
     mm_menu_0325123755_0_4.bgColor='#FFFFFF';
     mm_menu_0325123755_0_4.menuBorder=1;
     mm_menu_0325123755_0_4.menuLiteBgColor='#FFFFFF';
     mm_menu_0325123755_0_4.menuBorderBgColor='#000000';
  window.mm_menu_0325123755_0 = new Menu("root",172,18,"Arial, Helvetica, sans-serif",12,"#FFFFFF","#000099","#000000","#FFCC33","left","middle",3,0,200,-5,7,true,true,true,0,false,true);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_1);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_2);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_3);
  mm_menu_0325123755_0.addMenuItem(mm_menu_0325123755_0_4);
   mm_menu_0325123755_0.hideOnMouseOut=true;
   mm_menu_0325123755_0.childMenuIcon="arrows.gif";
   mm_menu_0325123755_0.bgColor='#FFFFFF';
   mm_menu_0325123755_0.menuBorder=1;
   mm_menu_0325123755_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0325123755_0.menuBorderBgColor='#000000';

                                window.mm_menu_0401121634_0 = new Menu("root",105,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,false,true);
  mm_menu_0401121634_0.addMenuItem("Earth","location='http://www.alexandria.ucsb.edu/other-sites/Earth.html'");
  mm_menu_0401121634_0.addMenuItem("Jupiter","location='http://www.alexandria.ucsb.edu/other-sites/Jupiter.html'");
  mm_menu_0401121634_0.addMenuItem("Mars","location='http://www.alexandria.ucsb.edu/other-sites/Mars.html'");
  mm_menu_0401121634_0.addMenuItem("Mercury","location='http://www.alexandria.ucsb.edu/other-sites/Mercury.html'");
  mm_menu_0401121634_0.addMenuItem("Moon","location='http://www.alexandria.ucsb.edu/other-sites/Moon.html'");
  mm_menu_0401121634_0.addMenuItem("Saturn","location='http://www.alexandria.ucsb.edu/other-sites/Saturn.html'");
  mm_menu_0401121634_0.addMenuItem("Sun","location='http://www.alexandria.ucsb.edu/other-sites/Sun.html'");
  mm_menu_0401121634_0.addMenuItem("Venus","location='http://www.alexandria.ucsb.edu/other-sites/Venus.html'");
   mm_menu_0401121634_0.fontWeight="bold";
   mm_menu_0401121634_0.hideOnMouseOut=true;
   mm_menu_0401121634_0.bgColor='#FFFFFF';
   mm_menu_0401121634_0.menuBorder=1;
   mm_menu_0401121634_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0401121634_0.menuBorderBgColor='#CCCCCC';
                  window.mm_menu_0401123714_0_1_1 = new Menu("A-B",105,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_1.addMenuItem("Algeria","location='http://www.alexandria.ucsb.edu/other-sites/Algeria.html'");
      mm_menu_0401123714_0_1_1.addMenuItem("Angola","location='http://www.alexandria.ucsb.edu/other-sites/Angola.html'");
      mm_menu_0401123714_0_1_1.addMenuItem("Benin","location='http://www.alexandria.ucsb.edu/other-sites/Benin.html'");
      mm_menu_0401123714_0_1_1.addMenuItem("Botswana","location='http://www.alexandria.ucsb.edu/other-sites/Botswana.html'");
      mm_menu_0401123714_0_1_1.addMenuItem("Burkina&nbsp;Faso","location='http://www.alexandria.ucsb.edu/other-sites/Burkina_Faso.html'");
      mm_menu_0401123714_0_1_1.addMenuItem("Burundi","location='http://www.alexandria.ucsb.edu/other-sites/Burundi.html'");
       mm_menu_0401123714_0_1_1.fontWeight="bold";
       mm_menu_0401123714_0_1_1.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_1.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_1.menuBorder=1;
       mm_menu_0401123714_0_1_1.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_1.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_2 = new Menu("C-D",173,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_2.addMenuItem("Cameroon","location='http://www.alexandria.ucsb.edu/other-sites/Cameroon.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Cape&nbsp;Verdi","location='http://www.alexandria.ucsb.edu/other-sites/Cape_Verde.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Central&nbsp;African&nbsp;Republic","location='http://www.alexandria.ucsb.edu/other-sites/Central_African_Republic.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Chad","location='http://www.alexandria.ucsb.edu/other-sites/Chad.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Comoros","location='http://www.alexandria.ucsb.edu/other-sites/Comoros.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Congo","location='http://www.alexandria.ucsb.edu/other-sites/Congo.html'");
      mm_menu_0401123714_0_1_2.addMenuItem("Djibouti","location='http://www.alexandria.ucsb.edu/other-sites/Djibouti.html'");
       mm_menu_0401123714_0_1_2.fontWeight="bold";
       mm_menu_0401123714_0_1_2.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_2.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_2.menuBorder=1;
       mm_menu_0401123714_0_1_2.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_2.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_3 = new Menu("E-G",131,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_3.addMenuItem("Egypt","location='http://www.alexandria.ucsb.edu/other-sites/Egypt.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Eritrea","location='http://www.alexandria.ucsb.edu/other-sites/Eritrea.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Equatorial&nbsp;Guinea","location='http://www.alexandria.ucsb.edu/other-sites/Equatorial_Guinea.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Ethiopia","location='http://www.alexandria.ucsb.edu/other-sites/Ethiopia.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Gabon","location='http://www.alexandria.ucsb.edu/other-sites/Gabon.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Gambia","location='http://www.alexandria.ucsb.edu/other-sites/Gambia.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Ghana","location='http://www.alexandria.ucsb.edu/other-sites/Ghana.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Guinea&nbsp;Bissau","location='http://www.alexandria.ucsb.edu/other-sites/Guinea_Bissau.html'");
      mm_menu_0401123714_0_1_3.addMenuItem("Guinea","location='http://www.alexandria.ucsb.edu/other-sites/Guinea.html'");
       mm_menu_0401123714_0_1_3.fontWeight="bold";
       mm_menu_0401123714_0_1_3.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_3.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_3.menuBorder=1;
       mm_menu_0401123714_0_1_3.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_3.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_4 = new Menu("I-L",96,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_4.addMenuItem("Ivory&nbsp;Coast","location='http://www.alexandria.ucsb.edu/other-sites/Ivory_Coast.html'");
      mm_menu_0401123714_0_1_4.addMenuItem("Kenya","location='http://www.alexandria.ucsb.edu/other-sites/Kenya.html'");
      mm_menu_0401123714_0_1_4.addMenuItem("Lesotho","location='http://www.alexandria.ucsb.edu/other-sites/Lesotho.html'");
      mm_menu_0401123714_0_1_4.addMenuItem("Liberia","location='http://www.alexandria.ucsb.edu/other-sites/Liberia.html'");
      mm_menu_0401123714_0_1_4.addMenuItem("Libya","location='http://www.alexandria.ucsb.edu/other-sites/Libya.html'");
       mm_menu_0401123714_0_1_4.fontWeight="bold";
       mm_menu_0401123714_0_1_4.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_4.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_4.menuBorder=1;
       mm_menu_0401123714_0_1_4.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_4.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_5 = new Menu("M-N",103,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_5.addMenuItem("Madagascar","location='http://www.alexandria.ucsb.edu/other-sites/Madagascar.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Malawi","location='http://www.alexandria.ucsb.edu/other-sites/Malawi.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Mali","location='http://www.alexandria.ucsb.edu/other-sites/Mali.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Mauritania","location='http://www.alexandria.ucsb.edu/other-sites/Mauritania.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Mauritius","location='http://www.alexandria.ucsb.edu/other-sites/Mauritius.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Morocco","location='http://www.alexandria.ucsb.edu/other-sites/Morocco.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Mozambique","location='http://www.alexandria.ucsb.edu/other-sites/Mozambique.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Namibia","location='http://www.alexandria.ucsb.edu/other-sites/Namibia.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Niger","location='http://www.alexandria.ucsb.edu/other-sites/Niger.html'");
      mm_menu_0401123714_0_1_5.addMenuItem("Nigeria","location='http://www.alexandria.ucsb.edu/other-sites/Nigeria.html'");
       mm_menu_0401123714_0_1_5.fontWeight="bold";
       mm_menu_0401123714_0_1_5.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_5.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_5.menuBorder=1;
       mm_menu_0401123714_0_1_5.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_5.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_6 = new Menu("R-S",102,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_6.addMenuItem("Rwanda","location='http://www.alexandria.ucsb.edu/other-sites/Rwanda.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Senegal","location='http://www.alexandria.ucsb.edu/other-sites/Senegal.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Seychelles","location='http://www.alexandria.ucsb.edu/other-sites/Seychelles.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Sierra&nbsp;Leone","location='http://www.alexandria.ucsb.edu/other-sites/Sierra_Leone.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Somalia","location='http://www.alexandria.ucsb.edu/other-sites/Somalia.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("South&nbsp;Africa","location='http://www.alexandria.ucsb.edu/other-sites/South_Africa.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Sudan","location='http://www.alexandria.ucsb.edu/other-sites/Sudan.html'");
      mm_menu_0401123714_0_1_6.addMenuItem("Swaziland","location='http://www.alexandria.ucsb.edu/other-sites/Swaziland.html'");
       mm_menu_0401123714_0_1_6.fontWeight="bold";
       mm_menu_0401123714_0_1_6.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_6.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_6.menuBorder=1;
       mm_menu_0401123714_0_1_6.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_6.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_1_7 = new Menu("T-Z",121,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_1_7.addMenuItem("Tanzania","location='http://www.alexandria.ucsb.edu/other-sites/Tanzania.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Togo","location='http://www.alexandria.ucsb.edu/other-sites/Togo.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Tunisia","location='http://www.alexandria.ucsb.edu/other-sites/Tunisia.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Uganda","location='http://www.alexandria.ucsb.edu/other-sites/Uganda.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Western&nbsp;Sahara","location='http://www.alexandria.ucsb.edu/other-sites/Western_Sahara.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Zambia","location='http://www.alexandria.ucsb.edu/other-sites/Zambia.html'");
      mm_menu_0401123714_0_1_7.addMenuItem("Zimbabwe","location='http://www.alexandria.ucsb.edu/other-sites/Zimbabwe.html'");
       mm_menu_0401123714_0_1_7.fontWeight="bold";
       mm_menu_0401123714_0_1_7.hideOnMouseOut=true;
       mm_menu_0401123714_0_1_7.bgColor='#FFFFFF';
       mm_menu_0401123714_0_1_7.menuBorder=1;
       mm_menu_0401123714_0_1_7.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_1_7.menuBorderBgColor='#CCCCCC';
    window.mm_menu_0401123714_0_1 = new Menu("Africa&nbsp;",56,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_1);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_2);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_3);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_4);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_5);
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_6,"location='http://www.alexandria.ucsb.edu/other-sites/Africa.html'");
    mm_menu_0401123714_0_1.addMenuItem(mm_menu_0401123714_0_1_7);
     mm_menu_0401123714_0_1.fontWeight="bold";
     mm_menu_0401123714_0_1.hideOnMouseOut=true;
     mm_menu_0401123714_0_1.childMenuIcon="arrows.gif";
     mm_menu_0401123714_0_1.bgColor='#FFFFFF';
     mm_menu_0401123714_0_1.menuBorder=1;
     mm_menu_0401123714_0_1.menuLiteBgColor='#FCFBFA';
     mm_menu_0401123714_0_1.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_2_1 = new Menu("A-E",94,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_2_1.addMenuItem("Argentina","location='http://www.alexandria.ucsb.edu/other-sites/Argentina.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("Brazil","location='http://www.alexandria.ucsb.edu/other-sites/Brazil.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("Canada","location='http://www.alexandria.ucsb.edu/other-sites/Canada.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("Colombia","location='http://www.alexandria.ucsb.edu/other-sites/Colombia.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("Costa&nbsp;Rica","location='http://www.alexandria.ucsb.edu/other-sites/Costa_Rica.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("Ecuador","location='http://www.alexandria.ucsb.edu/other-sites/Ecuador.html'");
      mm_menu_0401123714_0_2_1.addMenuItem("El&nbsp;Salvador","location='http://www.alexandria.ucsb.edu/other-sites/El_Salvador.html'");
       mm_menu_0401123714_0_2_1.fontWeight="bold";
       mm_menu_0401123714_0_2_1.hideOnMouseOut=true;
       mm_menu_0401123714_0_2_1.bgColor='#FFFFFF';
       mm_menu_0401123714_0_2_1.menuBorder=1;
       mm_menu_0401123714_0_2_1.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_2_1.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_2_2 = new Menu("G-Z",135,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_2_2.addMenuItem("Galapogos&nbsp;Islands","location='http://www.alexandria.ucsb.edu/other-sites/Galapogos_Islands.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Guatemala","location='http://www.alexandria.ucsb.edu/other-sites/Guatemala_Islands.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Honduras","location='http://www.alexandria.ucsb.edu/other-sites/Honduras.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Mexico","location='http://www.alexandria.ucsb.edu/other-sites/Mexico.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Nicaragua","location='http://www.alexandria.ucsb.edu/other-sites/Nicaragua.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Peru","location='http://www.alexandria.ucsb.edu/other-sites/Peru.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("United&nbsp;States","location='http://www.alexandria.ucsb.edu/other-sites/United_States.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Uruguay","location='http://www.alexandria.ucsb.edu/other-sites/Uruguay.html'");
      mm_menu_0401123714_0_2_2.addMenuItem("Venuzuela","location='http://www.alexandria.ucsb.edu/other-sites/Venezuela.html'");
       mm_menu_0401123714_0_2_2.fontWeight="bold";
       mm_menu_0401123714_0_2_2.hideOnMouseOut=true;
       mm_menu_0401123714_0_2_2.bgColor='#FFFFFF';
       mm_menu_0401123714_0_2_2.menuBorder=1;
       mm_menu_0401123714_0_2_2.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_2_2.menuBorderBgColor='#CCCCCC';
    window.mm_menu_0401123714_0_2 = new Menu("Americas",50,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
    mm_menu_0401123714_0_2.addMenuItem(mm_menu_0401123714_0_2_1);
    mm_menu_0401123714_0_2.addMenuItem(mm_menu_0401123714_0_2_2);
     mm_menu_0401123714_0_2.fontWeight="bold";
     mm_menu_0401123714_0_2.hideOnMouseOut=true;
     mm_menu_0401123714_0_2.childMenuIcon="arrows.gif";
     mm_menu_0401123714_0_2.bgColor='#FFFFFF';
     mm_menu_0401123714_0_2.menuBorder=1;
     mm_menu_0401123714_0_2.menuLiteBgColor='#FCFBFA';
     mm_menu_0401123714_0_2.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_3_1 = new Menu("A-I",95,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_3_1.addMenuItem("Burma","location='http://www.alexandria.ucsb.edu/other-sites/Burma.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("Cambodia","location='http://www.alexandria.ucsb.edu/other-sites/Cambodia.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("China","location='http://www.alexandria.ucsb.edu/other-sites/China.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("Hong&nbsp;Kong","location='http://www.alexandria.ucsb.edu/other-sites/Hong_Kong.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("India","location='http://www.alexandria.ucsb.edu/other-sites/India.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("Indonesia","location='http://www.alexandria.ucsb.edu/other-sites/Indonesia.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("Iraq","location='http://www.alexandria.ucsb.edu/other-sites/Iraq.html'");
      mm_menu_0401123714_0_3_1.addMenuItem("Israel","location='http://www.alexandria.ucsb.edu/other-sites/Israel.html'");
       mm_menu_0401123714_0_3_1.fontWeight="bold";
       mm_menu_0401123714_0_3_1.hideOnMouseOut=true;
       mm_menu_0401123714_0_3_1.bgColor='#FFFFFF';
       mm_menu_0401123714_0_3_1.menuBorder=1;
       mm_menu_0401123714_0_3_1.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_3_1.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_3_2 = new Menu("J-P",168,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_3_2.addMenuItem("Japan","location='http://www.alexandria.ucsb.edu/other-sites/Japan.html'");
      mm_menu_0401123714_0_3_2.addMenuItem("Jordan","location='http://www.alexandria.ucsb.edu/other-sites/Jordan.html'");
      mm_menu_0401123714_0_3_2.addMenuItem("Lebanon","location='http://www.alexandria.ucsb.edu/other-sites/Lebanon.html'");
      mm_menu_0401123714_0_3_2.addMenuItem("Malaysia","location='http://www.alexandria.ucsb.edu/other-sites/Malaysia.html'");
      mm_menu_0401123714_0_3_2.addMenuItem("North&nbsp;Korea","location='http://www.alexandria.ucsb.edu/other-sites/North_Korea.html'");
      mm_menu_0401123714_0_3_2.addMenuItem("Papua&nbsp;and&nbsp;New&nbsp;Guinea","location='http://www.alexandria.ucsb.edu/other-sites/Papua_and_New_Guinea.html'");
       mm_menu_0401123714_0_3_2.fontWeight="bold";
       mm_menu_0401123714_0_3_2.hideOnMouseOut=true;
       mm_menu_0401123714_0_3_2.bgColor='#FFFFFF';
       mm_menu_0401123714_0_3_2.menuBorder=1;
       mm_menu_0401123714_0_3_2.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_3_2.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_3_3 = new Menu("R-Z",102,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_3_3.addMenuItem("Russia","location='http://www.alexandria.ucsb.edu/other-sites/Russia.html'");
      mm_menu_0401123714_0_3_3.addMenuItem("South&nbsp;Korea","location='http://www.alexandria.ucsb.edu/other-sites/South_Korea.html'");
      mm_menu_0401123714_0_3_3.addMenuItem("Syria","location='http://www.alexandria.ucsb.edu/other-sites/Syria.html'");
      mm_menu_0401123714_0_3_3.addMenuItem("Thailand","location='http://www.alexandria.ucsb.edu/other-sites/Thailand.html'");
      mm_menu_0401123714_0_3_3.addMenuItem("Tibet","location='http://www.alexandria.ucsb.edu/other-sites/Tibet.html'");
      mm_menu_0401123714_0_3_3.addMenuItem("Vietnam","location='http://www.alexandria.ucsb.edu/other-sites/Vietnam.html'");
       mm_menu_0401123714_0_3_3.fontWeight="bold";
       mm_menu_0401123714_0_3_3.hideOnMouseOut=true;
       mm_menu_0401123714_0_3_3.bgColor='#FFFFFF';
       mm_menu_0401123714_0_3_3.menuBorder=1;
       mm_menu_0401123714_0_3_3.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_3_3.menuBorderBgColor='#CCCCCC';
    window.mm_menu_0401123714_0_3 = new Menu("Asia",49,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
    mm_menu_0401123714_0_3.addMenuItem(mm_menu_0401123714_0_3_1);
    mm_menu_0401123714_0_3.addMenuItem(mm_menu_0401123714_0_3_2);
    mm_menu_0401123714_0_3.addMenuItem(mm_menu_0401123714_0_3_3);
     mm_menu_0401123714_0_3.fontWeight="bold";
     mm_menu_0401123714_0_3.hideOnMouseOut=true;
     mm_menu_0401123714_0_3.childMenuIcon="arrows.gif";
     mm_menu_0401123714_0_3.bgColor='#FFFFFF';
     mm_menu_0401123714_0_3.menuBorder=1;
     mm_menu_0401123714_0_3.menuLiteBgColor='#FCFBFA';
     mm_menu_0401123714_0_3.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_4_1 = new Menu("A-D",170,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_4_1.addMenuItem("Andorra","location='http://www.alexandria.ucsb.edu/other-sites/Andorra.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Austria","location='http://www.alexandria.ucsb.edu/other-sites/Austria.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Belarus","location='http://www.alexandria.ucsb.edu/other-sites/Belarus.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Belgium","location='http://www.alexandria.ucsb.edu/other-sites/Belgium.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Bosnia&nbsp;and&nbsp;Herzegovina","location='http://www.alexandria.ucsb.edu/other-sites/Bosnia_and_Herzegovina.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Croatia","location='http://www.alexandria.ucsb.edu/other-sites/Croatia.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Czech&nbsp;Republic","location='http://www.alexandria.ucsb.edu/other-sites/Czech_Republic.html'");
      mm_menu_0401123714_0_4_1.addMenuItem("Denmark","location='http://www.alexandria.ucsb.edu/other-sites/Denmark.html'");
       mm_menu_0401123714_0_4_1.fontWeight="bold";
       mm_menu_0401123714_0_4_1.hideOnMouseOut=true;
       mm_menu_0401123714_0_4_1.bgColor='#FFFFFF';
       mm_menu_0401123714_0_4_1.menuBorder=1;
       mm_menu_0401123714_0_4_1.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_4_1.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_4_2 = new Menu("E-H",274,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_4_2.addMenuItem("Estonia","location='http://www.alexandria.ucsb.edu/other-sites/Estonia.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Finland","location='http://www.alexandria.ucsb.edu/other-sites/Finland.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Former&nbsp;Yugoslav&nbsp;Republic&nbsp;of&nbsp;Macedonia","location='http://www.alexandria.ucsb.edu/other-sites/Former_Yugoslav_Republic_of_Macedonia.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("France","location='http://www.alexandria.ucsb.edu/other-sites/France.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Germany","location='http://www.alexandria.ucsb.edu/other-sites/Germany.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Greece","location='http://www.alexandria.ucsb.edu/other-sites/Greece.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Guernsey","location='http://www.alexandria.ucsb.edu/other-sites/Guernsey.html'");
      mm_menu_0401123714_0_4_2.addMenuItem("Hungary","location='http://www.alexandria.ucsb.edu/other-sites/Hungary.html'");
       mm_menu_0401123714_0_4_2.fontWeight="bold";
       mm_menu_0401123714_0_4_2.hideOnMouseOut=true;
       mm_menu_0401123714_0_4_2.bgColor='#FFFFFF';
       mm_menu_0401123714_0_4_2.menuBorder=1;
       mm_menu_0401123714_0_4_2.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_4_2.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_4_3 = new Menu("I-N",102,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_4_3.addMenuItem("Iceland","location='http://www.alexandria.ucsb.edu/other-sites/Iceland.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Ireland","location='http://www.alexandria.ucsb.edu/other-sites/Ireland.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Italy","location='http://www.alexandria.ucsb.edu/other-sites/Italy.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Latvia","location='http://www.alexandria.ucsb.edu/other-sites/Latvia.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Liechtenstein","location='http://www.alexandria.ucsb.edu/other-sites/Liechtenstein.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Lithuania","location='http://www.alexandria.ucsb.edu/other-sites/Lithuania.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Luxembourg","location='http://www.alexandria.ucsb.edu/other-sites/Luxembourg.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Malta","location='http://www.alexandria.ucsb.edu/other-sites/Malta.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Netherlands","location='http://www.alexandria.ucsb.edu/other-sites/Netherlands.html'");
      mm_menu_0401123714_0_4_3.addMenuItem("Norway","location='http://www.alexandria.ucsb.edu/other-sites/Norway.html'");
       mm_menu_0401123714_0_4_3.fontWeight="bold";
       mm_menu_0401123714_0_4_3.hideOnMouseOut=true;
       mm_menu_0401123714_0_4_3.bgColor='#FFFFFF';
       mm_menu_0401123714_0_4_3.menuBorder=1;
       mm_menu_0401123714_0_4_3.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_4_3.menuBorderBgColor='#CCCCCC';
      window.mm_menu_0401123714_0_4_4 = new Menu("O-Z",167,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
      mm_menu_0401123714_0_4_4.addMenuItem("Poland","location='http://www.alexandria.ucsb.edu/other-sites/Poland.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Portugal","location='http://www.alexandria.ucsb.edu/other-sites/Portugal.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Serbia&nbsp;and&nbsp;Montenegro","location='http://www.alexandria.ucsb.edu/other-sites/Serbia_and_Montenegro.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Slovakia","location='http://www.alexandria.ucsb.edu/other-sites/Slovakia.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Slovenia","location='http://www.alexandria.ucsb.edu/other-sites/Slovenia.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Spain","location='http://www.alexandria.ucsb.edu/other-sites/Spain.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Sweden","location='http://www.alexandria.ucsb.edu/other-sites/Sweden.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Switzerland","location='http://www.alexandria.ucsb.edu/other-sites/Switzerland.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("Ukraine","location='http://www.alexandria.ucsb.edu/other-sites/Ukraine.html'");
      mm_menu_0401123714_0_4_4.addMenuItem("United&nbsp;Kingdom","location='http://www.alexandria.ucsb.edu/other-sites/United_Kingdom.html'");
       mm_menu_0401123714_0_4_4.fontWeight="bold";
       mm_menu_0401123714_0_4_4.hideOnMouseOut=true;
       mm_menu_0401123714_0_4_4.bgColor='#FFFFFF';
       mm_menu_0401123714_0_4_4.menuBorder=1;
       mm_menu_0401123714_0_4_4.menuLiteBgColor='#FCFBFA';
       mm_menu_0401123714_0_4_4.menuBorderBgColor='#CCCCCC';
    window.mm_menu_0401123714_0_4 = new Menu("Europe",52,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
    mm_menu_0401123714_0_4.addMenuItem(mm_menu_0401123714_0_4_1);
    mm_menu_0401123714_0_4.addMenuItem(mm_menu_0401123714_0_4_2);
    mm_menu_0401123714_0_4.addMenuItem(mm_menu_0401123714_0_4_3);
    mm_menu_0401123714_0_4.addMenuItem(mm_menu_0401123714_0_4_4);
     mm_menu_0401123714_0_4.fontWeight="bold";
     mm_menu_0401123714_0_4.hideOnMouseOut=true;
     mm_menu_0401123714_0_4.childMenuIcon="arrows.gif";
     mm_menu_0401123714_0_4.bgColor='#FFFFFF';
     mm_menu_0401123714_0_4.menuBorder=1;
     mm_menu_0401123714_0_4.menuLiteBgColor='#FCFBFA';
     mm_menu_0401123714_0_4.menuBorderBgColor='#CCCCCC';
    window.mm_menu_0401123714_0_5 = new Menu("Oceania",106,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
    mm_menu_0401123714_0_5.addMenuItem("Australia","location='http://www.alexandria.ucsb.edu/other-sites/Australia.html'");
    mm_menu_0401123714_0_5.addMenuItem("Easter&nbsp;Island","location='http://www.alexandria.ucsb.edu/other-sites/Easter_Island.html'");
    mm_menu_0401123714_0_5.addMenuItem("Moorea","location='http://www.alexandria.ucsb.edu/other-sites/Moorea.html'");
    mm_menu_0401123714_0_5.addMenuItem("New&nbsp;Zealand","location='http://www.alexandria.ucsb.edu/other-sites/New_Zealand.html'");
    mm_menu_0401123714_0_5.addMenuItem("Samoa","location='http://www.alexandria.ucsb.edu/other-sites/Samoa.html'");
     mm_menu_0401123714_0_5.fontWeight="bold";
     mm_menu_0401123714_0_5.hideOnMouseOut=true;
     mm_menu_0401123714_0_5.bgColor='#FFFFFF';
     mm_menu_0401123714_0_5.menuBorder=1;
     mm_menu_0401123714_0_5.menuLiteBgColor='#FCFBFA';
     mm_menu_0401123714_0_5.menuBorderBgColor='#CCCCCC';
  window.mm_menu_0401123714_0 = new Menu("root",86,14,"Arial, Helvetica, sans-serif",12,"#000066","#FFFFFF","#FFFFFF","#000066","left","middle",1,1,200,-5,7,true,true,true,0,true,true);
  mm_menu_0401123714_0.addMenuItem(mm_menu_0401123714_0_1,"location='http://www.alexandria.ucsb.edu/other-sites/Africa.html'");
  mm_menu_0401123714_0.addMenuItem(mm_menu_0401123714_0_2);
  mm_menu_0401123714_0.addMenuItem("Antarctica","location='http://www.alexandria.ucsb.edu/other-sites/Antarctica.html'");
  mm_menu_0401123714_0.addMenuItem(mm_menu_0401123714_0_3);
  mm_menu_0401123714_0.addMenuItem(mm_menu_0401123714_0_4);
  mm_menu_0401123714_0.addMenuItem(mm_menu_0401123714_0_5);
   mm_menu_0401123714_0.fontWeight="bold";
   mm_menu_0401123714_0.hideOnMouseOut=true;
   mm_menu_0401123714_0.childMenuIcon="arrows.gif";
   mm_menu_0401123714_0.bgColor='#FFFFFF';
   mm_menu_0401123714_0.menuBorder=1;
   mm_menu_0401123714_0.menuLiteBgColor='#FCFBFA';
   mm_menu_0401123714_0.menuBorderBgColor='#CCCCCC';

mm_menu_0401123714_0.writeMenus();
} // mmLoadMenus()



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script language="JavaScript" src="../js/mm_menu.js"></script>
</head>

<body>
<div align="center"></div>
<table width="763" height="202"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th height="27" bgcolor="#000000" scope="row"><span class="style114">Additional Resources Available Through Alexandria Digital Library </span></th>
  </tr>
  <tr>
    <th height="164" bgcolor="#000000" scope="row"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="763" height="170" align="left">
      <param name="movie" value="../images/MIL/ExternalReLists/external_bar.swf">
      <param name="quality" value="high">
      <embed src="../images/MIL/ExternalReLists/external_bar.swf" width="763" height="170" align="left" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>
    </object></th>
  </tr>
</table>
<table width="99%" height="19"  border="0" cellpadding="0" cellspacing="0">
  <tr bgcolor="#999999">
    <th height="19" valign="top" bgcolor="#FFFFFF" scope="row"><div align="center"></div></th>
  </tr>
</table>
<span class="style51"><span class="style97">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><br>
<table width="99%"  border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="37%" height="380" scope="row"><div align="center">
    </div>      
      <p><span class="style51"><span class="style97">&nbsp; </span></span></p>
      <p>&nbsp;</p>
      <p><span class="style51"><span class="style97"><span class="style137">&nbsp;&nbsp;&nbsp; <br>
            </span></span><span class="style127"><span class="style126">THE ALEXANDRIA DIGITAL LIBRARY (ADL): <br>
        The Alexandria Digital Library offers easy access to its collections<br>
            </span></span></span><span class="style128">of geographically referenced materials. The library; headquartered <br>
            at the University of California , Santa Barbara; is hosted by the <br>
            Davidson Library's Map and Imagery Lab. The MIL team is working <br>
            with development teams throughout the world to establish remote, <br>
            independent, yet federated ADL nodes featuring local geographically <br>
    referenced collections. </span> </p>
    <p align="left"><a href="http://clients.alexandria.ucsb.edu"><img src="../images/MIL/adl.jpg" alt="Alexandria Digital Library" width="71" height="73" border="0"></a></p>    </td>
    <td width="63%" scope="row"><table width="77%" height="311"  border="1" align="right" cellpadding="1" cellspacing="1" bordercolor="#000000">
      <tr>
        <th height="242" valign="top" background="../images/MIL/ExternalReLists/idols.jpg" scope="row"><p align="center"><span class="style134 style3">Oceania</span></p>
          <p align="center">&nbsp;</p>          
          <p align="center" class="style3">&nbsp;&nbsp;&nbsp; &nbsp;Australia &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Easter Island &nbsp;&nbsp;&nbsp;&nbsp;Moorea&nbsp;&nbsp;&nbsp;&nbsp; </p>
          <p align="center" class="style3">New&nbsp;Zealand&nbsp;&nbsp;&nbsp;&nbsp;Samoa </p></th>
        </tr>
    </table>
    <div align="right"></div></td>
  </tr>
</table>
<table width="99%" height="19"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th width="91%" height="19" scope="row"><div align="center"><span class="style113"><span class="style3"></span></span></div>      <span class="style11"><a href="http://www.alexandria.ucsb.edu/other-sites/Universe.html">Universe&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://www.alexandria.ucsb.edu/other-sites/Earth.html">Earth</a><a href="http://www.alexandria.ucsb.edu/other-sites/Africa.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Africa</a><a href="http://www.alexandria.ucsb.edu/other-sites/Americas.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Americas</a><a href="http://www.alexandria.ucsb.edu/other-sites/Antarctica.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Antarctica&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://www.alexandria.ucsb.edu/other-sites/Asia.html">Asia</a><a href="http://www.alexandria.ucsb.edu/other-sites/Europe.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Europe</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.alexandria.ucsb.edu/other-sites/Oceania.html">Oceania</a></span></th>
  </tr>
</table>
<br>
<table width="99%"  border="0" cellpadding="0" cellspacing="0" class="style102">
  <tr>
    <th colspan="3" class="style51 style130" scope="row"><p align="center" class="style102 style132 style133">The MIL is a closed-stack collection; most MIL materials do not circulate.<br>
    Fees for access to the imagery and digital data collection are charged to those persons not performing University of California related research.<br>
    Copyright &copy; 2007 University of California Regents; All Rights Reserved.</p></th>
  </tr>
  <tr>
    <th width="25%" height="67" valign="top" scope="row"><div align="left">
      <p class="style132">Last Updated<br>
        <!-- #BeginDate format:Am1m -->October 6, 2008  12:15<!-- #EndDate -->
</p>
    </div></th>
    <th width="55%" scope="row">&nbsp;</th>
    <th width="20%" scope="row"><div align="right"><span class="style131">Map and Imagery Lab,<br>
  Davidson Library<br>
  University of California, <br>
  Santa Barbara, CA</span><span class="style63"><span class="style101"><br>
  93106-9010</span></span></div></th>
  </tr>
</table>
<p align="center" class="style102">&nbsp;</p>
</body>
</html>
