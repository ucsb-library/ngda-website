<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Meetings</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	background-color: ffffffff;
}
.style1 {color: #000000}
.style174 {font-size: 14px}
a {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #333333;
	font-weight: bold;
}
a:link {
	text-decoration: none;
	color: #FFFFFF;
}
a:visited {
	text-decoration: none;
	color: #FFFFFF;
}
a:hover {
	text-decoration: none;
	color: #FF0000;
}
a:active {
	text-decoration: none;
}
.style206 {font-size: 12px}
.style16 {color: #666666}
.style23 {font-size: 10px}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000066;
}
.style67 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #000099;}
.style107 {	font-family: Arial, Helvetica, sans-serif;
	font-style: italic;
	font-weight: bold;
	color: #000099;
}
.style110 {	color: #180289;
	font-size: 24px;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style81 {	font-size: 14px;
	font-family: "Times New Roman", Times, serif;
}
.style286 {font-family: "Times New Roman", Times, serif; color: #000099;}
.style289 {font-family: "Times New Roman", Times, serif; font-style: italic; font-weight: bold; color: #000099; font-size: 14px; }
.style295 {font-family: Arial, Helvetica, sans-serif}
.style296 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #000066; }
.style297 {color: #000066; font-size: 14px;}
.style298 {font-size: 14px; font-family: Arial, Helvetica, sans-serif; }
.style300 {color: #000000; font-family: Arial, Helvetica, sans-serif;}
.style301 {color: #FFFFFF}
.style302 {color: #FFFFFF; font-family: Arial, Helvetica, sans-serif; }
.style303 {font-family: Arial, Helvetica, sans-serif; color: #000099; }
.style304 {color: #000099}
.style308 {font-weight: bold; font-size: 12px; color: #CCCCCC;}
.style312 {font-weight: bold; color: #FFFFFF;}
.style314 {
	color: #FF6600;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style315 {color: #FFFFFF; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
.style316 {font-weight: bold; font-family: Arial, Helvetica, sans-serif;}
.style176 {font-size: 11px}
.style177 {font-weight: bold; font-style: italic; color: #CCCCCC;}
.style183 {color: #FFFFFF; font-size: 12px; }
.style184 {color: #000066}
-->
</style>
<script language="JavaScript">
<!--
function mmLoadMenus() {
  if (window.mm_menu_0524113534_0) return;
          window.mm_menu_0524113534_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0524113534_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='#DPW'");
  mm_menu_0524113534_0.addMenuItem("Collections&nbsp;Workshop","location='#CW'");
  mm_menu_0524113534_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='#EAB'");
   mm_menu_0524113534_0.fontWeight="bold";
   mm_menu_0524113534_0.hideOnMouseOut=true;
   mm_menu_0524113534_0.bgColor='#000000';
   mm_menu_0524113534_0.menuBorder=1;
   mm_menu_0524113534_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0524113534_0.menuBorderBgColor='#999999';
    window.mm_menu_0524141709_0 = new Menu("root",171,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0524141709_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0524141709_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0524141709_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0524141709_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='research.php#RR'");
   mm_menu_0524141709_0.fontWeight="bold";
   mm_menu_0524141709_0.hideOnMouseOut=true;
   mm_menu_0524141709_0.bgColor='#000000';
   mm_menu_0524141709_0.menuBorder=1;
   mm_menu_0524141709_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0524141709_0.menuBorderBgColor='#999999';
window.mm_menu_0525110132_0 = new Menu("root",152,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0525110132_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0525110132_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0525110132_0.addMenuItem("2005&nbsp;LoC&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0525110132_0.fontWeight="bold";
   mm_menu_0525110132_0.hideOnMouseOut=true;
   mm_menu_0525110132_0.bgColor='#000000';
   mm_menu_0525110132_0.menuBorder=1;
   mm_menu_0525110132_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0525110132_0.menuBorderBgColor='#999999';

          window.mm_menu_0713134339_0 = new Menu("root",171,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713134339_0.addMenuItem("Journals&nbsp;and&nbsp;Papers","location='papers.php'");
  mm_menu_0713134339_0.addMenuItem("Technical&nbsp;Architecture","location='research.php#TA'");
  mm_menu_0713134339_0.addMenuItem("Format&nbsp;Registry","location='research.php#FR'");
  mm_menu_0713134339_0.addMenuItem("Collection&nbsp;Development","location='research.php#CD'");
  mm_menu_0713134339_0.addMenuItem("Rights&nbsp;&&nbsp;Responsibilities","location='research.php#CD'");
   mm_menu_0713134339_0.hideOnMouseOut=true;
   mm_menu_0713134339_0.bgColor='#555555';
   mm_menu_0713134339_0.menuBorder=1;
   mm_menu_0713134339_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713134339_0.menuBorderBgColor='#777777';
window.mm_menu_0713140001_0 = new Menu("root",141,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140001_0.addMenuItem("NGDA&nbsp;Participants","location='NGDA Participants.php#NGDA'");
  mm_menu_0713140001_0.addMenuItem("NDIIPP&nbsp;Partners","location='NGDA Participants.php#NDIIPP'");
  mm_menu_0713140001_0.addMenuItem("2005&nbsp;Awardees","location='NGDA Participants.php#2005'");
   mm_menu_0713140001_0.hideOnMouseOut=true;
   mm_menu_0713140001_0.bgColor='#555555';
   mm_menu_0713140001_0.menuBorder=1;
   mm_menu_0713140001_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140001_0.menuBorderBgColor='#777777';
  window.mm_menu_0713140152_0 = new Menu("root",182,17,"Arial, Helvetica, sans-serif",11,"#FFFFFF","#000099","#000099","#FFFFFF","left","middle",3,0,200,-5,7,true,true,true,0,true,true);
  mm_menu_0713140152_0.addMenuItem("Data&nbsp;Providers&nbsp;Workshop","location='meetings.php#DPW'");
  mm_menu_0713140152_0.addMenuItem("Collections&nbsp;Workshop","location='meetings.php#CW'");
  mm_menu_0713140152_0.addMenuItem("External&nbsp;Advisory&nbsp;Board","location='meetings.php#EAB'");
   mm_menu_0713140152_0.hideOnMouseOut=true;
   mm_menu_0713140152_0.bgColor='#555555';
   mm_menu_0713140152_0.menuBorder=1;
   mm_menu_0713140152_0.menuLiteBgColor='#FFFFFF';
   mm_menu_0713140152_0.menuBorderBgColor='#777777';

mm_menu_0713140152_0.writeMenus();
} // mmLoadMenus()

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript" src="js/mm_menu.js"></script>
</head>

<body onLoad="MM_preloadImages('images/templates/nb_home.jpg','images/templates/nb_home.jpg','images/templates/nb_home.jpg','images/templates/nb_research.jpg','images/templates/nb_research.jpg','images/templates/nb_research.jpg')">
<script language="JavaScript1.2">mmLoadMenus();</script>
<table width="769" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="720"><div align="center"><img src="images/Templates/New%20Banner.jpg" name="image1" width="760" height="203" border="0" usemap="#Map" id="image1"> </div></td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="317,155,382,170" href="research.php" alt="Research" onMouseOver="MM_showMenu(window.mm_menu_0713134339_0,317,190,null,'image1');MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_startTimeout();;MM_swapImgRestore()">
  <area shape="rect" coords="403,156,476,172" href="project_archive/NGDA_participants.php" alt="Participants" onMouseOver="MM_showMenu(window.mm_menu_0713140001_0,403,190,null,'image1');MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_startTimeout();MM_swapImgRestore()">
  <area shape="rect" coords="501,156,567,172" href="meetings.php" alt="Meetings" onMouseOver="MM_showMenu(window.mm_menu_0713140152_0,501,190,null,'image1');MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_startTimeout();MM_swapImgRestore()">
  <area shape="rect" coords="235,156,283,173" href="index.php" alt="Home" onMouseOver="MM_swapImage('image1','','images/templates/nb_home.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="587,161,662,175" href="contact.php" alt="Contact Us" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="689,157,743,169" href="http://www.digitalpreservation.gov/" alt="NDIIPP" onMouseOver="MM_swapImage('image1','','images/templates/nb_research.jpg',1)" onMouseOut="MM_swapImgRestore()">
</map>
<table width="769"  border="0" cellpadding="5" cellspacing="0" bordercolor="#0000FF">
  <tr align="right" bordercolor="#000033">
    <td width="19%" valign="top" background="images/templates/sidebar.jpg" bgcolor="#000099"><div align="center" class="style308">
        <div align="left" class="style301">
          <table width="100%"  border="3" align="center" cellpadding="3" cellspacing="1">
            <tr>
              <td><div align="left"><span class="style315">Presentations&nbsp;From March Meetin<strong>g</strong> </span></div></td>
            </tr>
            <tr>
              <td height="29">&nbsp;</td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/1_Janee.ppt">What We're Doing<br>
&amp; Why We're Here</a> </span></td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/5_Frew.ppt">ES3: Earth <br>
                System Science Server</a></span></td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/8_Squibb.ppt">COSMOS Strong-Motion VIRTUAL DATA CENTER </a></span></td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/9_Ratzesberger.ppt">Archiving <br>
                NEES Geospatial<br>
                Data </a></span></td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/6_Smorul_Channan.ppt">Digital Preservation <br>
&amp; Archiving at IACS</a></span></td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/7_Morris.ppt">NC Geospatial Data Archiving Project</a></span></td>
            </tr>
            <tr>
              <td><a href="wkshop_presentations/2005_03_data_providers/4_Zaslavsky.ppt">SDSC Projects</a> </td>
            </tr>
            <tr>
              <td><a href="wkshop_presentations/2005_03_data_providers/3_Hart.pdf">CaSIL</a></td>
            </tr>
            <tr>
              <td><a href="wkshop_presentations/2005_03_data_providers/2_Tait.ppt">GIS on Demand</a> </td>
            </tr>
            <tr>
              <td><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/10_Sweetkind-Singer.ppt">Cartography <br>
                Associates</a></span></td>
            </tr>
          </table>
        </div>
      </div>
        <p align="left" class="style1"><br>
          <br>
      </p>
        <table width="100%"  border="3" align="center" cellpadding="3" cellspacing="1">
          <tr>
            <td><div align="left"><span class="style315">Presentations&nbsp;From <br>
              September Meetin<strong>g</strong> </span></div></td>
          </tr>
          <tr>
            <td height="29"><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_03_data_providers/6_Smorul_Channan.ppt">Digital Cartographic Materials at LoC</a></span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/3.Garcia_Diane.ppt">USGS NGDA Activities</a> </span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/7.Johnson_JM.ppt">Collecting Digital Geospatial Data</a></span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/4.Knutzen_Matt.ppt">NYPL Map Divisions <br>
            Digital Collections</a> </span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/8.Shawa_Wangyal.ppt">CDPs for Digital Maps, Princeton</a> </span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/Schoenthaler.ppt">NGDA Copyright Considerations </a></span></div></td>
          </tr>
          <tr>
            <td><div align="left"><span class="style1"><a href="wkshop_presentations/2005_09_collections/Carver_Larry.ppt">Geospatial Network Vision </a></span></div></td>
          </tr>
        </table>
        <p align="left" class="style1"><br>
        </p>
        <div align="left">
          <p align="left" class="style302">&nbsp;</p>
          <p align="left" class="style302">&nbsp;</p>
      </div>
        <p>&nbsp;</p>
        <table width="100%"  border="3" cellspacing="1" cellpadding="3">
          <tr>
            <td><div align="left" class="style314">
              <span class="style301">&nbsp;NDIIPP Structure Planning Power Point Maps<br>
            from Fall , 2006 Meetin<span class="style316">g</span></span></div></td>
          </tr>
          <tr>
            <td height="29"><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"><a href="wkshop_presentations/2006_11_advisory_board/CarverPPT.jpg">NDIIPP Partners Community</a></div></td>
          </tr>
          <tr>
            <td><div align="left"><a href="wkshop_presentations/2006_11_advisory_board/Partners Map.jpg">NDIIPP Partners<br>
            Map</a></div></td>
          </tr>
          <tr>
            <td><div align="left"><a href="wkshop_presentations/2006_11_advisory_board/NGDA External Advisory Board Meeting.pdf" class="style206">Meeting Notes</a></div></td>
          </tr>
        </table>
        <p align="left" class="style1">&nbsp;</p>
        <p align="left" class="style1">&nbsp;</p>
        <p align="left" class="style1">&nbsp;</p>
        <p align="left" class="style1">&nbsp;</p>
        <p align="left" class="style1">&nbsp;</p>
        <p align="left" class="style1">&nbsp;</p>
        <div align="left">
          <p>&nbsp;</p>
          <p><br>
          </p>
        </div>
        <div align="center">
          <p align="left" class="style1">&nbsp;</p>
    </div></td>
    <td width="81%" valign="top"><div align="left"><span class="style110">Meetings</span>
        <div align="left"></div>
        <div align="left">
          <p class="style296">A principal goal of the National Geospatial Digital Archive project is to assess means for long-term federation of digital archivists. Hosting meetings, as well as attending those that bring together a variety of collections specialists, technical architects and data providers expand our awareness as to how federation might best be acheived.</p>
        </div>
        <p class="style296">&nbsp;</p>
        <p class="style296">We have been compiling information from presentations and discussions that can later be reviewed and integrated into our work. In addition to overviews of our meetings, many of the guests' presentations are attached.</p>
      </div>
        <div align="left">
          <p>_________________________</p>
          <div align="left"><span class="style289"><span class="style295">Data Providers Workshop</span></span><span class="style295"><a name="DPW"></a><br>
                <span class="style304">March 2005<br>
          Santa Barbara, Ca </span></span> </div>
          <p align="left">&nbsp;</p>
          <p align="left" class="style296">In March of 2005 members of professional and academic communities were invited to <br>
          participate in the <em>NGDA 2005 Data Providers Workshop. </em>The input of these individuals <br>
          served as invaluable resources as the breadth of data provider issues was revealed. </p>
          <p align="left" class="style296">Sharing the nature and experiences of their work with geospatial data, and allowing an open <br>
          forum for questions and discussion, provided critical affirmation of much of our <br>
          understanding as we push forward. </p>
          <p align="left" class="style296">The content, additionally, alerted us to potential stumbling blocks in pursuit of the <br>
          development of a digital archive.</p>
          <p align="left" class="style296">In this summary our expectations and questions are reflected as well as some of the pertinent <br>
          responses to them. As the necessary growth of this field is occurring at a highly rapid rate our <br>
          presenters were asked to encapsulate a vast array of experiences into relatively brief <br>
          presentations. Consequently, every contributed idea, concept, incident, and interaction <br>
          cannot be echoed here. </p>
          <p align="left" class="style296">Finally, the wisdom, wit, commitment, and sacrifices shared by each of the guests can only be <br>
          reflected through our thanks and hopes that this assembly brought a measure of value akin to <br>
          that which we received.</p>
        </div>
        <div align="left">
          <p><span class="style289"><br>
                <br>
          _____________________<br>
          <span class="style295">Collections Workshop</span></span><span class="style295"><a name="CW"></a><br>
          <span class="style304">September 2005<br>
          Palo Alto, Ca</span></span> </p>
          <p>&nbsp;</p>
          <p class="style296">NGDA held a day and a half workshop at Stanford University on September 14th and 15th, <br>
          2005 to determine and discuss the major collections issues for building an archive of digital geospatial data.<br>
          <br>
          Representation was provided from many agencies and institutions that bear responsibility for <br>
          creating, maintaining, and/or enabling access to a diverse range of geospatial data. A broad <br>
          array of participants from universities, government agencies, state archives, and commercial <br>
          firms attended.<br>
          <br>
          These guests were sought because of their expertise and interest in handling materials of a <br>
          related nature. Incorporating their input into the work of the NGDA is considered vital to not only <br>
          building a robust archive, but in the creation of a strong collection development policy. <br>
          The contributions of our guests provided help toward achieving these overall goals.</p>
          <br>
          <br>
        </div>
        <div align="left">
          <p><br>
              <br>
          </p>
          <div align="left">
            <p>______________________________________<br>
                <span class="style107"><span class="style174">NGDA External Advisory Board Meeting</span><a name="EAB"></a><br>
                </span><span class="style303">Policies and Governance <br>
            Fall 2006<br>
            South San Francisco, CA </span><span class="style286"><br>
          </span></p>
          </div>
          <p align="left" class="style296">By 2006 we have created repository environments designed to ingest, persistently store, <br>
          and access materials. We had begun construction on a geospatial format registry to <br>
          help define and explain the geospatial formats being archived. We established a collection development policy that will govern collaborative selection decisions. Finally, we were <br>
          completing the creation of a content provider agreement to governs the rights for, and responsibilities of, those who seek to deposit data into the NGDA and the archive's <br>
          content custodians. An external group of colleagues was convened to help us look into the<br>
          short-term future for guidance on policies and governance structures as we begin to <br>
          increase the network of repositories archiving geospatial data. We began discussion the following broad topics: </p>
          <div align="left"></div>
          <ul class="style295">
            <li class="style297">
              <p align="left">How do we attract others into the network? What do we expect repository partners <br>
              to be able to do? Should there be levels of participation? What are the guidelines <br>
              that should be created for those wanting to simply deposit data rather than be a <br>
              node? </p>
            </li>
            <li class="style297">
              <p align="left">What alliances should we try to form with: existing geospatial networks, <br>
              government agencies (federal and state), commercial companies, individuals, etc.? </p>
            </li>
            <li class="style297">
              <p align="left">Do we need a long-term governance structure? What are viable alternatives to that? </p>
            </li>
          </ul>
          <p align="left"><span class="style298">The attached notes provide much of the input from our guests stemming from these <br>
          questions</span></p>
          <p class="style296">&nbsp;</p>
      </div></td>
  </tr>
</table>
<br>
<table width="770"  border="0" cellspacing="0" cellpadding="5">
  <tr align="left">
    <td colspan="2" align="left" valign="top"><span class="style81"> </span> </td>
  </tr>
  <tr align="left" bgcolor="#000066">
    <td colspan="2" valign="top"><span class="style67"><span class="style16"><span class="style1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span> </td>
  </tr>
  <tr align="left">
    <td colspan="2" valign="top">&nbsp;</td>
  </tr>
  <tr align="left" bgcolor="#000066">
    <td colspan="2" valign="top">&nbsp;</td>
  </tr>
  <tr align="left" valign="middle">
    <td colspan="2"><div align="center"><span class="style67"><span class="style16"><span class="style1"><img src="images/templates/PhaseIIBottom.jpg" width="740" height="90"> &nbsp;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="style67"><span class="style16"></span></span></div></td>
  </tr>
  <tr>
    <td width="35%" align="center" valign="top"><div align="left"><span class="style164 style322 style23 style295"><font color="#CCCCCC" size="1"><strong>Copyright 
        &copy; 2005-2009<br>
        University of California,<br>
        Santa Barbara, CA 93106 <br>
        (805) 893-8000</strong></font></span></div></td>
    <td width="65%" align="center" valign="top"><div align="right"><span class="style164 style322 style23 style295"><font color="#CCCCCC" size="1"><strong>Last 
        Modified<br>
        <!-- #BeginDate format:Am1a -->January 22, 2009 1:17 PM<!-- #EndDate -->
        </strong> </font> </span></div></td>
  </tr>
</table>
<table width="770"  border="0" cellspacing="0" cellpadding="5">
  <tr align="left"> 
    <td align="left" valign="top"><span class="style81"> </span> <span class="style67"><span class="style16"><span class="style1"> 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span> 
      <div align="center"><span class="style67"><span class="style16"><span class="style1"> 
        &nbsp;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <table width="165" border="1" cellpadding="3" cellspacing="3" bordercolor="#0000CC">
          <tr> 
            <td><span class="style81"><a href="http://www.universityofcalifornia.edu/news/globalwarming.pdf"><img src="images/index/global%20warming.jpg" alt="UC Global Warming Initiative" width="400" height="92" border="0" align="middle"></a></span></td>
          </tr>
        </table>
        <span class="style67"><span class="style16"></span></span></div>
      <div align="left"></div>
      <div align="right"><span class="style164 style322 style23 style295"> </span></div></td>
  </tr>
</table>
<p>&nbsp;</p>
<div align="center"></div>
<p><span class="style81"></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align="left"></div>
</body>
</html>
