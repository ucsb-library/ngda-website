<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>NGDA - The National Geospatial Digital Archive</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="stylesheets/fluid.gs.css" type="text/css" media="screen" charset="utf-8" />
<!--[if lt IE 8]><link rel="stylesheet" href="stylesheets/fluid.gs.lt_ie8.css" type="text/css" media="screen" title="no title" charset="utf-8"><![endif]-->
<link href="stylesheets/ngda.css" media="screen" rel="stylesheet" charset="utf-8" type="text/css" />
	<link rel="stylesheet" href="qchange/css/qc_css.css" />
<!-- load Jquery library from Google api -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="qchange/js/quickchange.js"></script>
<!-- InstanceBeginEditable name="head" -->


<!-- InstanceEndEditable -->
</head>

<body>
<div class="fluid_grid_layout"> 
<!-- Header -->
<div class="six_column section">
	<div class="four column mid">
	<div class="column_content">
		<h1 class="">The National Geospatial Digital Archive</h1>
<!-- InstanceBeginEditable name="subhead" -->
		<h2 id="subhead">A Collaborative Initiative</h2>
<!-- InstanceEndEditable -->
		</div>
	</div> 
	<div class="two column">
	<div class="column_content">
		<a href="home.html" class="plain"><img src="images/ngda_logo.png" alt="NGDA Logo" width="170" height="140" /></a></div>
	</div> 
</div>



<div class="six_column section">
<!-- Left Navigation -->
	<div class="two column">
    <div class="column_content" id="navigation">
		<ul>
			<li><a href="home.html">Home</a></li>
		  <li><a href="news.html">News</a></li>
			<li><a href="research.html">Research</a></li>
			<li><a href="reports.html">Reports</a></li>
			<li><a href="join.php">Join</a></li>
		  <li><a href="contact.html">Contact Us</a></li>
			<li><a href="participants.html">Federation Members</a></li>
		  <li><a href="project_archive.html">Project Archive</a></li>
			
	      <br />&nbsp;
		</ul>
	</div>
<div id="leftlower" class="round">
<div id="partnerLogos">
<ul>
	<li><a class="plain" title="University of California Santa Barbara"><img src="images/ucsb_logo.png" width="68" height="73" /></a></li>
	<li><a class="plain" title="Stanford University"><img src="images/stanford_logo_t.png" width="69" height="73" /></a></li>
	<li><a class="plain" title="Library of Congress"><img src="images/library_of_congress_logo.png" width="75" height="73" alt="Library of Congress Logo" /></a></li>
	<li><a class="plain" title="University of Tennessee"><img src="images/u_of_tennessee_logo.png" width="85" height="61" /></a></li>
	<li><a class="plain" title="Vanderbuilt University"><img src="images/vanderbuilt_university_logo.png" width="64" height="53" /></a></li>
</ul>
</div>
</div>

	</div><!-- end two column div -->


<!--  Main Content -->
	<div class="five column">
	<div class="column_content">
	<div id="main_content">
<!-- InstanceBeginEditable name="Content" -->
<h2>		Articles and Papers </h2>

	<ul>
	<li>
		<a href="http://www.digitalpreservation.gov/partners/pioneers/detail_carver.html"> Digital Preservation Pioneer: Larry Carver</a> 
		<br />
		<em> December 2008 </em> Library of Congress, Digital Preservation 
	</li>
</ul>

<ul>
	<li>
		<a href="http://www.alexandria.ucsb.edu/%7Egjanee/archive/2008/agu.pdf"> Preserving the Context of&nbsp;Science Data</a> 
		<br />
		<em> December 2008 </em> AGU Power Point 
		<br />
		(Janee, Frew) 
	</li>
</ul>
<ul>
	<li>
		<a href="http://www.alexandria.ucsb.edu/%7Egjanee/archive/2008/idcc.pdf"> Relay-supporting Archives: Requirements and Progress </a> <a href="http://www.alexandria.ucsb.edu/%7Egjanee/archive/2008/idcc.pdf"> 
		
		</a>
		
		</li>
<li>

		
		
		July 2008 Presentation to 4th International Digital Curation Conference 12/08 
		<em> (Janee, Frew) </em> 
	</li>
</ul>



<ul>
	<li>
		Paper: <a href="http://www.digitalpreservation.gov/"> A Data Model and Architecture for Long-term Preservation </a> 
		<br />
		April 2008 &nbsp;(Janee,Mathena,Frew) 
	</li>
</ul>
<ul>
	<li>
		<a href="reports/profiles_in_research.pdf"> NGDA-NDIIPP, Preserving the Nation's Endangered Digital Information </a> <a href="reports/profiles_in_research"> 
			<br />
		</a> Profiles in Research UCSB, 2006 
	</li>
</ul>
<ul>
	<li>
		<a href="http://www-sul.stanford.edu/depts/gis/Digital_pres_geospatial"> Digital Preservation of Geospatial Data</a> Library Trends, <em> Fall 2006</em>Vol. 55, Issue 2 
	</li>
</ul>


<ul>
	<li>
		<a href="reports/Overview%20ITB.ppt"> NDIIPP and NGDA, National Preservation for Digital Content</a> 
		<br />
		Presentation to the UCSB IT Executive Committee &nbsp;September, 2006 
	</li>
</ul>
<ul>
	<li>
		Presentations to ALA June, 2007 
	</li>
	<li>
		<a href="research/Collections/NGDA%20PRESENTATION_ccc6_2007-1.pdf"> Cataloging and Classification Committee </a> <a href="research/Collections/NGDA%20PRESENTATION_MCMDG_6_2007-2.pdf"> Map </a> 
	</li>
</ul>
<ul>
	<li>
		<a href="research/Collections/NGDA%20PRESENTATION_MCMDG_6_2007-2.pdf"> Collection Management Discussion Group </a> 
	</li>
</ul>
<ul>
	<li>
		<a href="research/Collections/NGDA%20PRESENTATION_FSIDG_6_2007-3.pdf"> Federal Spatial Information Discussion Group </a> 
	</li>
</ul>
<ul>
	<li>
		<a href="research/Collections/NGDA%20PRESENTATION_GEOTECH_6_2007-1.pdf"> GIS Discussion Group and GeoTech Committee </a> 
	</li>
</ul>
<!-- InstanceEndEditable -->
	</div>
	</div>
	</div>
</div>


<!-- Footer -->
<div class="six_column section">
	<div class="six column">
		<div class="column_content">
		<div class="footer">
Copyright <a class="plain" href="javascript:login_to_QC();">&copy;</a> 2005-2009 University of California, Santa Barbara, CA 93106</div>
		</div>
	</div> 
</div>










</div>
</body>
<!-- InstanceEnd --></html>
