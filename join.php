<?

header("Location: http://www.ngda.org/home.html", TRUE, 301);

exit;



// above code added 2012_09_11 16:12   see Jira DEV-786 

error_reporting(E_ERROR);

	require_once('sanitizept.inc.php');// filters from http://www.owasp.org


if($_SERVER['REQUEST_METHOD'] == 'POST'){

$recipient = "ngda-ucsb@library.ucsb.edu";
$ccrecipient = "Julie Sweetkind-Singer <sweetkind@stanford.edu>, <ilessing@library.ucsb.edu>";
$from = "NGDA Website <webmaster@ngda.org>";
$subject = "NGDA.org webform output";

$smtpserver = "ssmtp.library.ucsb.edu";
$name		= sanitize(stripslashes($_POST['name']), HTML);
$institution= sanitize(stripslashes($_POST['institution']), HTML);
$comments	= sanitize(stripslashes($_POST['comments']), HTML);

$email		= fsanitize_email(stripslashes($_POST['email']));


// The non-fill field in the form is to prevent spam
if(strlen($_POST['non-fill'] ) > 0){
	$error_flag = TRUE;
	$error_msg = "Do not enter anything in the last box of the form.";
	} 

// make sure they entered a valid email address
if($email === FALSE){
	$error_flag = TRUE;
	$error_msg = "Invalid Email Address";
	$email = sanitize(stripslashes($_POST['email'])	,email);
	} 


//	require a name
if(strlen(trim($name)) < 1){
	$error_flag = TRUE;
	$error_msg = "Please enter your Name.  It is a requried field";
	}


//	If no errors then compose and send email
if(!$error_flag){
	$email_body = "
NGDA Message from website.  User filled form.

Name: $name

Institution: $institution

Email: $email

Comments:
$comments


";

	// using pear library to send email		
	require_once "Mail.php";
	
	$headers = array ('From' => $from, 'To' => $recipient, 'Subject' => $subject, 'Cc' => $ccrecipient);
	$smtp = Mail::factory('smtp', array ('host' => $smtpserver, 'auth' => false ));
	$mail = $smtp->send($recipient, $headers, $email_body);
	
	if (PEAR::isError($mail)) {
		$error_msg = "There was a problem on the server sending your message.
		<a href=\"mailto:dev@library.ucsb.edu\">Please Notify the administrator</a>";
		}else{
		$mailsent = TRUE;
		}



	}
	



	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>
			Join - The National Geospatial Digital Archive (NGDA)

</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="stylesheets/fluid.gs.css" type="text/css" media="screen" charset="utf-8" />
<!--[if lt IE 8]><link rel="stylesheet" href="stylesheets/fluid.gs.lt_ie8.css" type="text/css" media="screen" title="no title" charset="utf-8"><![endif]-->
<link href="stylesheets/ngda.css" media="screen" rel="stylesheet" charset="utf-8" type="text/css" />
	<link rel="stylesheet" href="qchange/css/qc_css.css" />
<!-- load Jquery library from Google api -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="qchange/js/quickchange.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div class="fluid_grid_layout"> 
<!-- Header -->
<div class="six_column section">
	<div class="four column mid">
	<div class="column_content">
		<h1 class="">The National Geospatial Digital Archive</h1>
<!-- InstanceBeginEditable name="subhead" -->
		<h2 id="subhead">A Collaborative Initiative</h2>
<!-- InstanceEndEditable -->
		</div>
	</div> 
	<div class="two column">
	<div class="column_content">
		<a href="home.html" class="plain"><img src="images/ngda_logo.png" alt="NGDA Logo" width="170" height="140" /></a></div>
	</div> 
</div>



<div class="six_column section">
<!-- Left Navigation -->
	<div class="two column">
    <div class="column_content" id="navigation">
		<ul>
			<li><a href="home.html">Home</a></li>
		  
			<li><a href="research.html">Research</a></li>
			<li><a href="reports.html">Reports</a></li>
			<li><a href="join.php">Join</a></li>
		  <li><a href="contact.html">Contact Us</a></li>
			<li><a href="participants.html">Federation Members</a></li>
		  <li><a href="project_archive.html">Project Archive</a></li>
			
	      <br />&nbsp;
		</ul>
	</div>
<div id="leftlower" class="round">
<div id="partnerLogos">
<ul>
	<li><a class="plain" title="University of California Santa Barbara"><img src="images/ucsb_logo.png" width="68" height="73" /></a></li>
	<li><a class="plain" title="Stanford University"><img src="images/stanford_logo_t.png" width="69" height="73" /></a></li>
	<li><a class="plain" title="Library of Congress"><img src="images/library_of_congress_logo.png" width="75" height="73" alt="Library of Congress Logo" /></a></li>
	<li><a class="plain" title="University of Tennessee"><img src="images/u_of_tennessee_logo.png" width="85" height="61" /></a></li>
	<li><a class="plain" title="Vanderbuilt University"><img src="images/vanderbuilt_university_logo.png" width="64" height="53" /></a></li>
</ul>
</div>
</div>

	</div><!-- end two column div -->


<!--  Main Content -->
	<div class="five column">
	<div class="column_content">
	<div id="main_content">
<!-- InstanceBeginEditable name="Content" -->
<h1>Join NGDA</h1>
<? if($error_flag){ ?>
<div class="error round">
<? echo $error_msg ?>
</div>
<? } ?>

<? if($mailsent){ ?>
<p> Thanks for contacting NGDA.  Your information has been sent and someone will bin in touch soon. </p>
<?}else{ ?>

<p>
If you would like to join the NGDA Federation please contact us with the form below:<br />
</p>
<div id="form_container">
<form action="<?=$_SERVER['PHP_SELF'];?>" method="post" enctype="application/x-www-form-urlencoded" name="contactus_form" id="contactus_form">
  <label for="name">Name</label>
  <input type="text" name="name" id="name" tabindex="10" class="round" value="<? echo stripslashes(sanitize($name,HTML));?>" />
  <br />
  <label for="institution">Institution</label>
  <input type="text" name="institution" id="institution" tabindex="20"  class="round" value="<? echo stripslashes(sanitize($institution,HTML));?>" />
  <br />
  <label for="email">Email</label>
  <input type="text" name="email" id="email" tabindex="30" class="round" value="<?=$email?>" value="<? echo $email?>" />
  <br />
  <label for="comments" class="textarea">Comments</label>
  <textarea name="comments" id="comments" cols="45" rows="7" tabindex="40" class="round" ><? echo sanitize(stripslashes($comments),HTML);?></textarea>
  <br />
  <label for="non-fill"></label>
  <input type="text" name="non-fill" id="non-fill" />
  <br />

<input type="submit" name="submit" id="submit" value="Submit" tabindex="50" />
</form>
</div>
<? } ?>
<br />
<br />
<br />

<p><a href="policies.html">Policies & Contracts</a></p>





<!-- InstanceEndEditable -->
	</div>
	</div>
	</div>
</div>


<!-- Footer -->
<div class="six_column section">
	<div class="six column">
		<div class="column_content">
		<div class="footer">
Copyright <a class="plain" href="javascript:login_to_QC();">&copy;</a> 2005-2009 University of California, Santa Barbara, CA 93106</div>
		</div>
	</div> 
</div>










</div>
<?
// if($_SERVER['REQUEST_METHOD'] == 'POST'){
// 	echo "<pre>";//debug
// 
// 	print_r($_POST);
// 	print_r($_SERVER);                        
// 
// 	echo "</pre>";//debug
// 	}
?>
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://analytics.library.ucsb.edu/" : "http://analytics.library.ucsb.edu/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://analytics.library.ucsb.edu/piwik.php?idsite=2" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tag --></body>
<!-- InstanceEnd --></html>
