<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>UCSB &amp; Stanford University -  NGDA - National Geospatial Digital Archive  - part of NDIIPP</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1"><LINK 
href="../stylesheets/ndiipp.css" type=text/css rel=stylesheet>
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<style type="text/css">
<!--
a:link {
	color: #000066;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #000099;
}
a:hover {
	text-decoration: underline;
	color: #0000FF;
}
a:active {
	text-decoration: none;
}
body {
	background-color: #FFFFFF;
	margin-left: 15px;
}
a {
	font-size: 12px;
	color: #000066;
	font-family: Arial, Helvetica, sans-serif;
}
.style45 {color: #000000; font-size: 14px; font-family: Georgia, "Times New Roman", Times, serif; }
.style51 {color: #000000; font-size: 12px; font-family: Georgia, "Times New Roman", Times, serif; }
.style57 {font-family: Arial, Helvetica, sans-serif}
.style58 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #000000;}
.style59 {font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.style60 {color: #000000}
.style61 {font-size: 12px}
.style63 {font-size: 14px}
-->
</style>
</HEAD>
<BODY topMargin=0 
MARGINHEIGHT="0">
<table width="750" height="100%" align="left" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="5" valign="top"><img src="file:///N|/images/Templates/banner.jpg" width="458" height="35"></td>
    <td width="211" rowspan="4" align="right" valign="top" bgcolor="#4F5151">
      <div align="left"><img src="file:///N|/Templates/left%20banner%20copy.jpg" width="210" height="593"></div></td>
  </tr>
  <tr>
    <td width="109" bgcolor="#FFFFFF"><div align="center"><a href="file:///N|/index.php">Home</a></div></td>
    <td width="123" bgcolor="#FFFFFF"><div align="center"><a href="file:///N|/reports.php">Research</a></div></td>
    <td width="116" bgcolor="#FFFFFF"><div align="center"><a href="file:///N|/Meetings.htm">Meetings</a></div></td>
    <td width="128" bgcolor="#FFFFFF"><div align="center"><a href="file:///N|/participants.php">Participants</a></div></td>
    <td width="58" bgcolor="#FFFFFF"><div align="center"><a href="file:///N|/contact.php"> &nbsp;Contacts</a></div></td>
  </tr>
  <tr>
    <td height="271" colspan="5" align="left" valign="top"><p align="left" class="style59">&nbsp;</p>
        <p align="left" class="style59">From the first colonial maps to the satellite imagery of the 21st century, cartographic information has helped define and frame our view of the United States. </p>
        <p class="style59">Geospatial imagery is now a critical tool for state and federal researchers pursuing greater understandings of complex matters including:<br>
            <br>
        Disaster Relief, Census Data, and Land Use Management <br>
        <br>
        Concerned that millions of nationally important digital information resources are in danger of being lost or corrupted, the Library of Congress has formed The National Digital Information Infrastructure and Preservation Program (NDIIPP).<br>
        <br>
        As partners in the project the university libraries of UCSB and Stanford are leading the formation of the National Geospatial Digital Archive (NGDA), a collecting network for the archiving of geospatial images and data. They have partnered with eight institutions to begin a three year, $15 million effort to begin construction of a nationwide digital collection and preservation system.</p>
        <p class="style59">The objectives of the project are to: </p>
        <div align="left">
          <div align="left">
            <ul>
              <li><span class="style59">Create a new national federated network committed to archiving geospatial imagery and data. </span>
              <li class="style58"><span class="style59">Investigate the proper and optimal roles of such a federated archive, with consideration of distant (dark) backup and migration, directly serving content to users, vs. referring requestors back to the originators of the data for copies or assistance, active or passive quality/integrity monitoring, application of metadata, federated searching, dissemination of metadata, etc.</span>
              <li class="style59">Collect and archive major segments of at-risk digital geospatial data and images.     
              <li>
                        <div align="left" class="style59">Develop best practices for the presentation of archived digital geospatial data.</div>
              </li>
                      <li>
                        <div align="left">
                          <div align="left" class="style59">Develop partner communication mechanisms for the project and then ongoing. <br>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div align="left">
                          <div align="left" class="style59">Develop a series of policy agreements governing retention, rights management, obligations of partners, interoperability of systems, exchange of digital objects, etc.</div>
                        </div>
                      </li>
            </ul>
          </div>
          <br>
          <br>
      </div></td>
  </tr>
  <tr>
    <td height="78" colspan="5" valign="top"><img src="file:///N|/images/Templates/bottom_title%20copy.jpg" width="550" height="38"></td>
  </tr>
</table>
</BODY>
</HTML>
